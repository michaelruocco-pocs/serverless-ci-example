package uk.co.tpplc.middleware;

import java.util.HashMap;
import java.util.Map;

public class FakeBaseEnvironment implements BaseEnvironment {

    private final Map<String, String> variables = new HashMap<>();

    public void setIntegrationReference(String integrationReference) {
        setVariable(INTEGRATION_REFERENCE, integrationReference);
    }

    public void setStage(String stage) {
        setVariable(STAGE, stage);
    }

    public void setAwsService(String awsService) {
        setVariable(AWS_SERVICE, awsService);
    }

    @Override
    public String getIntegrationReference() {
        return getVariable(INTEGRATION_REFERENCE);
    }

    @Override
    public String getStage() {
        return getVariable(STAGE);
    }

    @Override
    public String getAwsService() {
        return getVariable(AWS_SERVICE);
    }

    public void setVariable(String name, String value) {
        variables.put(name, value);
    }

    public String getVariable(String name) {
        return variables.get(name);
    }

}
