package uk.co.tpplc.middleware.receipt;

import uk.co.mruoc.log.EnvironmentVariableAppender;

import java.util.Arrays;
import java.util.List;

import static uk.co.tpplc.middleware.BaseEnvironment.AWS_SERVICE;
import static uk.co.tpplc.middleware.BaseEnvironment.INTEGRATION_REFERENCE;
import static uk.co.tpplc.middleware.BaseEnvironment.STAGE;

public class DefaultEnvironmentVariableAppender extends EnvironmentVariableAppender {

    private static final List<String> NAMES = Arrays.asList(
            INTEGRATION_REFERENCE,
            STAGE,
            AWS_SERVICE);

    public DefaultEnvironmentVariableAppender() {
        super(NAMES);
    }

}
