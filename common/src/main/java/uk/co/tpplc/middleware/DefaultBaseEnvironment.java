package uk.co.tpplc.middleware;

public class DefaultBaseEnvironment implements BaseEnvironment {

    @Override
    public String getIntegrationReference() {
        return getVariable(INTEGRATION_REFERENCE);
    }

    @Override
    public String getStage() {
        return getVariable(STAGE);
    }

    @Override
    public String getAwsService() {
        return getVariable(AWS_SERVICE);
    }

    public String getVariable(String name) {
        return System.getenv(name);
    }

}
