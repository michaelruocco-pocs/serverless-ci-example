package uk.co.tpplc.middleware.receipt;

import uk.co.tpplc.middleware.DefaultBaseEnvironment;

public class DefaultEnvironment extends DefaultBaseEnvironment implements Environment {

    @Override
    public String getCmmReceiptDynamoTableName() {
        return getVariable(CMM_RECEIPT_DYNAMO_TABLE_NAME);
    }

    @Override
    public String getBssLegacyReceiptBucketName() {
        return getVariable(BSS_LEGACY_RECEIPT_BUCKET_NAME);
    }

    @Override
    public String getCmmReceiptTopicArn() {
        return getVariable(CMM_RECEIPT_TOPIC_ARN);
    }

    @Override
    public String getPostReceiptUrl() {
        return getVariable(POST_RECEIPT_URL);
    }

    @Override
    public String getIntegrationReference() {
        return getVariable(INTEGRATION_REFERENCE);
    }

    @Override
    public String getStage() {
        return getVariable(STAGE);
    }

}
