package uk.co.tpplc.middleware;

public interface BaseEnvironment {

    String INTEGRATION_REFERENCE = "INTEGRATION_REFERENCE";
    String STAGE = "STAGE";
    String AWS_SERVICE = "AWS_SERVICE";

    String getIntegrationReference();

    String getStage();

    String getAwsService();

}
