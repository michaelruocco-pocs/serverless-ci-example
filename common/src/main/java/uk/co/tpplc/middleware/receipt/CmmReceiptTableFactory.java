package uk.co.tpplc.middleware.receipt;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import uk.co.mruoc.dynamo.JsonLogSimpleTable;
import uk.co.mruoc.dynamo.SimpleTable;
import uk.co.mruoc.dynamo.TableConfig;
import uk.co.mruoc.dynamo.TableConfig.TableConfigBuilder;
import uk.co.mruoc.log.Logger;

public class CmmReceiptTableFactory {

    public static TableConfig buildConfig(Environment environment) {
        return buildConfig(environment.getCmmReceiptDynamoTableName());
    }

    public static TableConfig buildConfig(String tableName) {
        return new TableConfigBuilder()
                .setTableName(tableName)
                .setIdFieldName("id")
                .build();
    }

    public static SimpleTable buildTable(Logger logger) {
        AmazonDynamoDB db = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_WEST_1).build();
        return new JsonLogSimpleTable(db, logger);
    }


}
