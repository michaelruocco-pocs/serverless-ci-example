package uk.co.tpplc.middleware.receipt;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import uk.co.mruoc.lambda.ApiGatewayRequest;
import uk.co.mruoc.lambda.ApiGatewayRequestSerializer;
import uk.co.mruoc.log.Logger;
import uk.co.mruoc.log.LoggerFactory;
import uk.co.mruoc.log.VariableAppender;

public class ReceiptLoggerFactory {

    private static final Gson GSON = new GsonBuilder()
            .disableHtmlEscaping()
            .serializeNulls()
            .registerTypeAdapter(ApiGatewayRequest.class, new ApiGatewayRequestSerializer())
            .create();

    private static final VariableAppender VARIABLE_APPENDER = new DefaultEnvironmentVariableAppender();

    public static Logger getLogger(Class<?> clazz) {
        return LoggerFactory.getLogger(clazz, GSON, VARIABLE_APPENDER);
    }

}
