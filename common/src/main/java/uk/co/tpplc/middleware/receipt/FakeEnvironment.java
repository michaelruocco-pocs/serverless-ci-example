package uk.co.tpplc.middleware.receipt;

import java.util.HashMap;
import java.util.Map;

public class FakeEnvironment implements Environment {

    private final Map<String, String> variables = new HashMap<>();

    public void setBssLegacyReceiptBucketName(String bucketName) {
        setVariable(BSS_LEGACY_RECEIPT_BUCKET_NAME, bucketName);
    }

    public void setCmmReceiptTopicArn(String arn) {
        setVariable(CMM_RECEIPT_TOPIC_ARN, arn);
    }

    public void setCmmReceiptDynamoTableName(String tableName) {
        setVariable(CMM_RECEIPT_DYNAMO_TABLE_NAME, tableName);
    }

    public void setIntegrationReference(String integrationReference) {
        setVariable(INTEGRATION_REFERENCE, integrationReference);
    }

    public void setPostReceiptUrl(String postReceiptUrl) {
        setVariable(POST_RECEIPT_URL, postReceiptUrl);
    }

    public void setStage(String stage) {
        setVariable(STAGE, stage);
    }

    @Override
    public String getCmmReceiptDynamoTableName() {
        return getVariable(CMM_RECEIPT_DYNAMO_TABLE_NAME);
    }

    @Override
    public String getBssLegacyReceiptBucketName() {
        return getVariable(BSS_LEGACY_RECEIPT_BUCKET_NAME);
    }

    @Override
    public String getCmmReceiptTopicArn() {
        return getVariable(CMM_RECEIPT_TOPIC_ARN);
    }

    @Override
    public String getPostReceiptUrl() {
        return getVariable(POST_RECEIPT_URL);
    }

    @Override
    public String getIntegrationReference() {
        return getVariable(INTEGRATION_REFERENCE);
    }

    @Override
    public String getStage() {
        return getVariable(STAGE);
    }

    @Override
    public String getAwsService() {
        return getVariable(AWS_SERVICE);
    }

    private void setVariable(String name, String value) {
        variables.put(name, value);
    }

    private String getVariable(String name) {
        return variables.get(name);
    }

}
