package uk.co.tpplc.middleware.receipt;

import uk.co.tpplc.middleware.BaseEnvironment;

public interface Environment extends BaseEnvironment {

    String BSS_LEGACY_RECEIPT_BUCKET_NAME = "BSS_LEGACY_RECEIPT_BUCKET_NAME";
    String CMM_RECEIPT_TOPIC_ARN = "CMM_RECEIPT_TOPIC_ARN";
    String CMM_RECEIPT_DYNAMO_TABLE_NAME = "CCM_RECEIPT_DYNAMO_TABLE_NAME";
    String POST_RECEIPT_URL = "POST_RECEIPT_URL";

    String getCmmReceiptDynamoTableName();

    String getBssLegacyReceiptBucketName();

    String getCmmReceiptTopicArn();

    String getPostReceiptUrl();

}
