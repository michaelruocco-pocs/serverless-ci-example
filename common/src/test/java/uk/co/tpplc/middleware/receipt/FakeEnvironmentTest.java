package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FakeEnvironmentTest {

    private final FakeEnvironment environment = new FakeEnvironment();

    @Test
    public void shouldReturnBssLegacyReceiptBucketName() {
        String bucketName = "bucket-name";
        environment.setBssLegacyReceiptBucketName(bucketName);

        assertThat(environment.getBssLegacyReceiptBucketName()).isEqualTo(bucketName);
    }

    @Test
    public void shouldReturnCmmReceiptTopicArn() {
        String arn = "aws:test:arn";
        environment.setCmmReceiptTopicArn(arn);

        assertThat(environment.getCmmReceiptTopicArn()).isEqualTo(arn);
    }

    @Test
    public void shouldReturnCmmReceiptDynamoTableName() {
        String tableName = "table-name";
        environment.setCmmReceiptDynamoTableName(tableName);

        assertThat(environment.getCmmReceiptDynamoTableName()).isEqualTo(tableName);
    }

    @Test
    public void shouldReturnIntegrationReference() {
        String integrationReference = "integration-reference";
        environment.setIntegrationReference(integrationReference);

        assertThat(environment.getIntegrationReference()).isEqualTo(integrationReference);
    }

    @Test
    public void shouldReturnStage() {
        String stage = "stage";
        environment.setStage(stage);

        assertThat(environment.getStage()).isEqualTo(stage);
    }

}
