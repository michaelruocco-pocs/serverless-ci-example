package uk.co.tpplc.middleware.receipt;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.co.tpplc.middleware.receipt.Environment.*;

public class DefaultEnvironmentTest {

    @Rule
    public final EnvironmentVariables mockEnvironment = new EnvironmentVariables();

    private final Environment environment = new DefaultEnvironment();

    @Test
    public void shouldReturnBssLegacyReceiptBucketName() {
        String expectedBucketName = "bucket-name";
        mockEnvironmentVariable(BSS_LEGACY_RECEIPT_BUCKET_NAME, expectedBucketName);

        String result = environment.getBssLegacyReceiptBucketName();

        assertThat(result).isEqualTo(expectedBucketName);
    }

    @Test
    public void shouldReturnCmmReceiptTopicArn() {
        String expectedArn = "aws:arn:value";
        mockEnvironmentVariable(CMM_RECEIPT_TOPIC_ARN, expectedArn);

        String result = environment.getCmmReceiptTopicArn();

        assertThat(result).isEqualTo(expectedArn);
    }

    @Test
    public void shouldReturnCmmReceiptDynamoTableName() {
        String expectedTableName = "dynamoTableName";
        mockEnvironmentVariable(CMM_RECEIPT_DYNAMO_TABLE_NAME, expectedTableName);

        String result = environment.getCmmReceiptDynamoTableName();

        assertThat(result).isEqualTo(expectedTableName);
    }

    @Test
    public void shouldReturnIntegrationReference() {
        String expectedIntegrationReference = "my-integration-reference";
        mockEnvironmentVariable(INTEGRATION_REFERENCE, expectedIntegrationReference);

        String result = environment.getIntegrationReference();

        assertThat(result).isEqualTo(expectedIntegrationReference);
    }

    @Test
    public void shouldReturnStage() {
        String stage = "my-stage";
        mockEnvironmentVariable(STAGE, stage);

        String result = environment.getStage();

        assertThat(result).isEqualTo(stage);
    }

    private void mockEnvironmentVariable(String name, String value) {
        mockEnvironment.set(name, value);
    }

}
