package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.mruoc.dynamo.TableConfig;

import static org.assertj.core.api.Assertions.assertThat;

public class CmmReceiptTableFactoryTest {

    private final FakeEnvironment environment = new FakeEnvironment();

    @Test
    public void shouldPopulateTableNameFromEnvironment() {
        String tableName = "table-name";
        environment.setCmmReceiptDynamoTableName(tableName);

        TableConfig config = CmmReceiptTableFactory.buildConfig(environment);

        assertThat(config.getTableName()).isEqualTo(tableName);
    }

    @Test
    public void shouldPopulateIdFieldName() {
        TableConfig config = CmmReceiptTableFactory.buildConfig(environment);

        assertThat(config.getIdFieldName()).isEqualTo("id");
    }

    @Test
    public void shouldPopulateDefaultWriteCapacityUnits() {
        TableConfig config = CmmReceiptTableFactory.buildConfig(environment);

        assertThat(config.getWriteCapacityUnits()).isEqualTo(1L);
    }

    @Test
    public void shouldPopulateDefaultReadCapacityUnits() {
        TableConfig config = CmmReceiptTableFactory.buildConfig(environment);

        assertThat(config.getReadCapacityUnits()).isEqualTo(1L);
    }

}
