package uk.co.tpplc.middleware.receipt;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.AmazonApiGatewayClientBuilder;
import com.amazonaws.services.apigateway.model.*;
import org.apache.log4j.Logger;

import java.util.List;

public class ApiIdFinder {

    private static final Logger LOG = Logger.getLogger(ApiIdFinder.class);

    private final AmazonApiGateway gateway = AmazonApiGatewayClientBuilder.standard()
            .withRegion(Regions.EU_WEST_1)
            .build();

    public String getId(String name) {
        GetRestApisRequest request = new GetRestApisRequest();
        GetRestApisResult result = gateway.getRestApis(request);
        List<RestApi> apis = result.getItems();
        for (RestApi api : apis) {
            LOG.info("got api " + api.getName());
            if (api.getName().equals(name)) {
                GetRestApiRequest apiRequest = new GetRestApiRequest();
                apiRequest.withRestApiId(api.getId());
                GetRestApiResult apiResult = gateway.getRestApi(apiRequest);
                return apiResult.getId();
            }
        }
        throw new RuntimeException("no api found with name " + name);
    }

}
