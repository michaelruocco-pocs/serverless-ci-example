package uk.co.tpplc.middleware.receipt;

public class TestResourceNames {

    private final ApiIdFinder apiIdFinder = new ApiIdFinder();

    private final String stage;
    private final String apiId;

    public TestResourceNames() {
        this(StageLoader.loadStage());
    }

    public TestResourceNames(String stage) {
        this.stage = stage;
        this.apiId = apiIdFinder.getId(stage + "-wma1");
    }

    public String getReceiptsUploadBucketName() {
        return stage + "-bss-receipt-upload";
    }

    public String getBssLegacyReceiptsBucketName() {
        return stage + "-bss-legacy-receipt";
    }

    public String getDynamoTableName() {
        return stage + "-cmm-receipt";
    }

    public String getApiEndpoint() {
        return "https://" + apiId + ".execute-api.eu-west-1.amazonaws.com/" + stage + "/receipts/";
    }

}
