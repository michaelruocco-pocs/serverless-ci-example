package uk.co.tpplc.middleware.receipt;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ArraySizeExtractor {

    public int extract(String json) {
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json);
        JsonArray array = element.getAsJsonArray();
        return array.size();
    }

}
