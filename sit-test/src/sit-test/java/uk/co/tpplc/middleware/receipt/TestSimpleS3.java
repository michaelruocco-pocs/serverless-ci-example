package uk.co.tpplc.middleware.receipt;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.slf4j.Logger;
import uk.co.mruoc.s3.LogSimpleS3;

public class TestSimpleS3 extends LogSimpleS3 {

    public TestSimpleS3(Logger logger) {
        super(build(), logger);
    }

    private static AmazonS3 build() {
        return AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_1).build();
    }

}
