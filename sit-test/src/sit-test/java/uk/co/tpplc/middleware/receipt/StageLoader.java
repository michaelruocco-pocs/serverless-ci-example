package uk.co.tpplc.middleware.receipt;

import org.apache.commons.lang3.StringUtils;

public class StageLoader {

    private static final String DEFAULT_STAGE = "sit";

    public static String loadStage() {
        String stage = System.getProperty("stage");
        if (StringUtils.isEmpty(stage))
            return DEFAULT_STAGE;
        return stage;
    }

}
