package uk.co.tpplc.middleware.receipt;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.mruoc.dynamo.LogSimpleTable;
import uk.co.mruoc.dynamo.SimpleTable;
import uk.co.mruoc.dynamo.TableConfig;
import uk.co.mruoc.properties.ClasspathFileContentLoader;
import uk.co.mruoc.properties.FileContentLoader;
import uk.co.mruoc.s3.BucketPoller;
import uk.co.mruoc.s3.ObjectDefinition;
import uk.co.mruoc.s3.SimpleS3;

import static org.assertj.core.api.Assertions.assertThat;

public class SitTest {

    private static final Logger LOG = LoggerFactory.getLogger(SitTest.class);

    private static final String INPUT_FILE_PATH = "/uk/co/tpplc/middleware/receipt/bss-receipt-valid.txt";
    private static final String EXPECTED_OUTPUT_FILE_PATH = "/uk/co/tpplc/middleware/receipt/expected-legacy-receipts.txt";
    private static final String OBJECT_KEY = "sit-test-file.txt";

    private final TestResourceNames env = new TestResourceNames();
    private final ArraySizeExtractor arraySizeExtractor = new ArraySizeExtractor();

    private final ObjectDefinition receiptsUploadBucketInfo = new ObjectDefinition(env.getReceiptsUploadBucketName(), OBJECT_KEY);
    private final ObjectDefinition legacyReceiptsBucketInfo = new ObjectDefinition(env.getBssLegacyReceiptsBucketName(), OBJECT_KEY);

    private final FileContentLoader contentLoader = new ClasspathFileContentLoader();
    private final String content = contentLoader.loadContent(INPUT_FILE_PATH);

    private final SimpleS3 fileSystem = new TestSimpleS3(LoggerFactory.getLogger(SitTest.class));
    private final BucketPoller bucketPoller = new BucketPoller(fileSystem);

    private final TableConfig tableConfig = CmmReceiptTableFactory.buildConfig(env.getDynamoTableName());
    private final SimpleTable table = buildTable();
    private final ApiPoller apiPoller = new ApiPoller();

    @Before
    public void setUp() {
        fileSystem.deleteObject(receiptsUploadBucketInfo);
        fileSystem.deleteObject(legacyReceiptsBucketInfo);
        table.clear(tableConfig);
    }

    @After
    public void tearDown() {
        fileSystem.deleteObject(receiptsUploadBucketInfo);
        fileSystem.deleteObject(legacyReceiptsBucketInfo);
        table.clear(tableConfig);
    }

    @Test
    public void shouldOutputLegacyLinesToS3Bucket() {
        String expectedOutput = contentLoader.loadContent(EXPECTED_OUTPUT_FILE_PATH);

        fileSystem.putObject(receiptsUploadBucketInfo, content);

        String output = bucketPoller.pollBucketForContent(legacyReceiptsBucketInfo);
        assertThat(output).isEqualTo(expectedOutput);
    }

    @Test
    public void shouldBeAbleToGetReceiptLineViaApi() {
        String receiptId = "0535022222";
        String endpoint = env.getApiEndpoint() + receiptId;

        fileSystem.putObject(receiptsUploadBucketInfo, content);

        uk.co.mruoc.http.client.Response response = apiPoller.pollQueueForResponse(endpoint);
        assertThat(response.getStatusCode()).isEqualTo(200);
        assertThat(response.getBody()).contains("\"ExternalGRN\":122202,");
    }

    @Test
    public void shouldBeAbleToGetAllReceiptLinesViaApi() {
        String endpoint = env.getApiEndpoint();

        fileSystem.putObject(receiptsUploadBucketInfo, content);

        uk.co.mruoc.http.client.Response response = apiPoller.pollQueueForResponseWithBodyArraySize(endpoint, 7);
        assertThat(response.getStatusCode()).isEqualTo(200);
        assertThat(arraySizeExtractor.extract(response.getBody())).isEqualTo(7);
    }

    private static SimpleTable buildTable() {
        AmazonDynamoDB db = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_WEST_1).build();
        return new LogSimpleTable(db, LOG);
    }

}
