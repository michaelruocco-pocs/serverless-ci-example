package uk.co.tpplc.middleware.receipt;

import uk.co.mruoc.http.client.HttpClient;
import uk.co.mruoc.http.client.SimpleHttpClient;
import uk.co.mruoc.retry.DefaultRetryConfig.DefaultRetryConfigBuilder;
import uk.co.mruoc.retry.RetryConfig;
import uk.co.mruoc.retry.Sleeper;

public class ApiPoller {

    private final Sleeper sleeper = new Sleeper();
    private final ArraySizeExtractor arraySizeExtractor = new ArraySizeExtractor();
    private final HttpClient client;
    private final int maxAttempts;
    private final int delay;

    public ApiPoller() {
        this(new SimpleHttpClient(), new DefaultRetryConfigBuilder().setMaxAttempts(40).build());
    }

    public ApiPoller(HttpClient client, RetryConfig retryConfig) {
        this.client = client;
        this.maxAttempts = retryConfig.getMaxAttempts();
        this.delay = retryConfig.getDelay();
    }

    public uk.co.mruoc.http.client.Response pollQueueForResponse(String endpoint) {
        uk.co.mruoc.http.client.Response response = client.get(endpoint);
        int attempts = 0;
        while(response.getStatusCode() != 200 && attempts < maxAttempts) {
            sleeper.sleep(delay);
            attempts++;
            response = client.get(endpoint);
        }
        return response;
    }

    public uk.co.mruoc.http.client.Response pollQueueForResponseWithBodyArraySize(String endpoint, int arraySize) {
        uk.co.mruoc.http.client.Response response = client.get(endpoint);
        int attempts = 0;
        while(response.getStatusCode() != 200 || extractArraySize(response.getBody()) != arraySize && attempts < maxAttempts) {
            sleeper.sleep(delay);
            attempts++;
            response = client.get(endpoint);
        }
        return response;
    }

    private int extractArraySize(String json) {
        return arraySizeExtractor.extract(json);
    }

}
