package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNS;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNSRecord;
import java.util.Collections;

public class FakeSnsEventBuilder {

    private String id;
    private String body;

    public FakeSnsEventBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public FakeSnsEventBuilder withBody(String body) {
        this.body = body;
        return this;
    }

    public SNSEvent build() {
        SNS sns = new SNS();
        sns.setMessage(body);
        sns.setMessageId(id);

        SNSRecord record = new SNSRecord();
        record.setSns(sns);

        SNSEvent snsEvent = new SNSEvent();
        snsEvent.setRecords(Collections.singletonList(record));

        return snsEvent;
    }

}
