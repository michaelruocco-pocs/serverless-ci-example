package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.mruoc.http.client.FakeHttpClient;

import static org.assertj.core.api.Assertions.assertThat;

public class ApiPersistorTest {

    private final FakeEnvironment environment = new FakeEnvironment();
    private final FakeHttpClient httpClient = new FakeHttpClient();

    private final ApiReceiptPersistor persistor = new ApiReceiptPersistor(environment, httpClient);

    @Test
    public void shouldPostToPostReceiptUrl() {
        String postReceiptUrl = "http://post-receipt/url";
        environment.setPostReceiptUrl(postReceiptUrl);
        httpClient.cannedResponse(200);

        persistor.persist("");

        assertThat(httpClient.lastRequestUri()).isEqualTo(postReceiptUrl);
    }

    @Test
    public void shouldPostJson() {
        String json = "{ body: \"value\" }";
        String postReceiptUrl = "http://post-receipt/url";
        environment.setPostReceiptUrl(postReceiptUrl);
        httpClient.cannedResponse(200);

        persistor.persist(json);

        assertThat(httpClient.lastRequestBody()).isEqualTo(json);
    }

}
