package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import org.junit.Test;
import uk.co.mruoc.lambda.LambdaResponse;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.co.tpplc.middleware.receipt.TestLines.*;

public class CmmReceiptPersistorHandlerTest {

    private final SNSEvent snsEvent = new FakeSnsEventBuilder().withId("id")
            .withBody(VALID_RECEIPT_LINE_XML)
            .build();
    private final FakeContext context = new FakeContext();
    private final FakeReceiptPersistor receiptPersistor = new FakeReceiptPersistor();

    private final CmmReceiptPersistorHandler handler = new CmmReceiptPersistorHandler(receiptPersistor);

    @Test
    public void shouldReturn200StatusCode() {
        LambdaResponse response = handler.handleRequest(snsEvent, context);

        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    public void shouldReturnJsonReceiptInResponseBody() {
        LambdaResponse response = handler.handleRequest(snsEvent, context);

        assertThat(response.getBody()).isEqualTo(VALID_RECEIPT_LINE_JSON);
    }

    @Test
    public void shouldPersistJsonReceipt() {
        handler.handleRequest(snsEvent, context);

        assertThat(receiptPersistor.getLastJson()).isEqualTo(VALID_RECEIPT_LINE_JSON);
    }

}
