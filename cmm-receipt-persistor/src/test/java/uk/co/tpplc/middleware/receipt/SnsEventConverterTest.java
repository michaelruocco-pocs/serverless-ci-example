package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SnsEventConverterTest {

    private static final String ID = "message_id";
    private static final String BODY = "message_body";

    private final SNSEvent snsEvent = new FakeSnsEventBuilder()
            .withId("message_id")
            .withBody("message_body")
            .build();

    private final SnsEventConverter converter = new SnsEventConverter();

    @Test
    public void shouldConvertId() {
        MessageInfo info = converter.toMessageInfo(snsEvent);
        assertThat(info.getId()).isEqualTo(ID);
    }

    @Test
    public void shouldConvertBody() {
        MessageInfo info = converter.toMessageInfo(snsEvent);
        assertThat(info.getBody()).isEqualTo(BODY);
    }

}
