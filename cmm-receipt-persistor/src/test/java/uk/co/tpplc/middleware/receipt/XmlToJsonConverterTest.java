package uk.co.tpplc.middleware.receipt;

import org.json.JSONObject;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class XmlToJsonConverterTest {

    private final XmlToJsonConverter converter = new XmlToJsonConverter();

    @Test
    public void shouldConvertXmlToJson() {
        String xml = "<xml><header>HEADER</header><value>VALUE</value></xml>";
        String expectedJson = "{\"xml\":{\"header\":\"HEADER\",\"value\":\"VALUE\"}}";

        String json = converter.toJson(xml);

        assertThat(json).isEqualTo(expectedJson);
    }

}
