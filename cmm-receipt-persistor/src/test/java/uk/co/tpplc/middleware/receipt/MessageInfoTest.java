package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MessageInfoTest {

    private static final String ID = "message-id";
    private static final String BODY = "body";

    private final MessageInfo info = new MessageInfo(ID, BODY);

    @Test
    public void shouldReturnId() {
        assertThat(info.getId()).isEqualTo(ID);
    }

    @Test
    public void shouldReturnBody() {
        assertThat(info.getBody()).isEqualTo(BODY);
    }

}
