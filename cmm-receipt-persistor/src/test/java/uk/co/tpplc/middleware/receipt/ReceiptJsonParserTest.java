package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.co.tpplc.middleware.receipt.TestLines.VALID_RECEIPT_LINE_JSON;

public class ReceiptJsonParserTest {

    private final ReceiptJsonParser parser = new ReceiptJsonParser();

    @Test
    public void shouldParseReceiptId() {
        String id = parser.parseId(VALID_RECEIPT_LINE_JSON);

        assertThat(id).isEqualTo("0462831111");
    }

}
