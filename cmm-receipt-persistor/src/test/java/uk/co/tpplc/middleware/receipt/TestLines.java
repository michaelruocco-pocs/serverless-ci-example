package uk.co.tpplc.middleware.receipt;

public class TestLines {

    public static final String VALID_RECEIPT_LINE_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" +
            "<DeliveryReceipt>" +
            "<ActualDeliveryDateTime>16-02-2017</ActualDeliveryDateTime>" +
            "<DocumentDateTime>15-02-2017</DocumentDateTime>" +
            "<ExternalGRN>MS422074</ExternalGRN>" +
            "<DeliveryReceiptLine>" +
            "<ReceivedQuantity>220.0</ReceivedQuantity>" +
            "<PurchaseOrderLineItem>" +
            "<LineNumber>1</LineNumber>" +
            "</PurchaseOrderLineItem>" +
            "<PurchaseOrder>" +
            "<Supplier>" +
            "<Id>SM8896</Id>" +
            "</Supplier>" +
            "<PurchaseOrderIds>" +
            "<Id>0462831111</Id>" +
            "<IdType>T</IdType>" +
            "</PurchaseOrderIds>" +
            "</PurchaseOrder>" +
            "</DeliveryReceiptLine>" +
            "</DeliveryReceipt>";

    public static final String VALID_RECEIPT_LINE_JSON = "{\"DeliveryReceipt\":" +
            "{\"ActualDeliveryDateTime\":\"16-02-2017\"," +
            "\"DeliveryReceiptLine\":" +
            "{\"ReceivedQuantity\":220," +
            "\"PurchaseOrder\":" +
            "{\"PurchaseOrderIds\":" +
            "{\"Id\":\"0462831111\"," +
            "\"IdType\":\"T\"}," +
            "\"Supplier\":{\"Id\":\"SM8896\"}}," +
            "\"PurchaseOrderLineItem\":{\"LineNumber\":1}}," +
            "\"ExternalGRN\":\"MS422074\"," +
            "\"DocumentDateTime\":\"15-02-2017\"}}";

}
