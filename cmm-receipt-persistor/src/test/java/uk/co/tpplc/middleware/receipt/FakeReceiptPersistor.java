package uk.co.tpplc.middleware.receipt;

public class FakeReceiptPersistor implements ReceiptPersistor {

    private String lastJson;

    @Override
    public void persist(String json) {
        this.lastJson = json;
    }

    public String getLastJson() {
        return lastJson;
    }

}
