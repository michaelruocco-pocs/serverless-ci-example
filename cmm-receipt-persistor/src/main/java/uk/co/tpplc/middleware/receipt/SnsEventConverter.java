package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNSRecord;

import java.util.List;

public class SnsEventConverter {

    public MessageInfo toMessageInfo(SNSEvent snsEvent) {
        List<SNSRecord> records = snsEvent.getRecords();
        SNSRecord record = records.get(0);
        SNSEvent.SNS sns = record.getSNS();
        return new MessageInfo(sns.getMessageId(), sns.getMessage());
    }

}
