package uk.co.tpplc.middleware.receipt;

import org.json.JSONObject;
import org.json.XML;
import uk.co.mruoc.log.Logger;

public class XmlToJsonConverter {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(XmlToJsonConverter.class);

    public String toJson(String xml) {
        logXml(xml);
        JSONObject json = XML.toJSONObject(xml);
        logJsonConversion(xml, json.toString());
        return json.toString();
    }

    private void logXml(String xml) {
        LOG.info().message("converting xml to json").field("xml", xml).log();
    }

    private void logJsonConversion(String xml, String json) {
        LOG.info().message("converted xml into json").field("xml", xml).field("json", json).log();
    }

}
