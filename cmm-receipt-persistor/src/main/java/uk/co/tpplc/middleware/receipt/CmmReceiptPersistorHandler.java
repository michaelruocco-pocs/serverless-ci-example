package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import uk.co.mruoc.lambda.LambdaResponse;
import uk.co.mruoc.lambda.OkLambdaResponse;
import uk.co.mruoc.log.Logger;

public class CmmReceiptPersistorHandler implements RequestHandler<SNSEvent, LambdaResponse> {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(CmmReceiptPersistorHandler.class);

    private final SnsEventConverter snsEventConverter = new SnsEventConverter();
    private final XmlToJsonConverter xmlToJsonConverter = new XmlToJsonConverter();
    private final ReceiptPersistor persistor;

    public CmmReceiptPersistorHandler() {
        this(new ApiReceiptPersistor(new DefaultEnvironment()));
    }

    public CmmReceiptPersistorHandler(ReceiptPersistor persistor) {
        this.persistor = persistor;
    }

    @Override
    public LambdaResponse handleRequest(SNSEvent snsEvent, Context context) {
        logEvent(snsEvent, context);
        MessageInfo info = snsEventConverter.toMessageInfo(snsEvent);
        LOG.info().message("got message").field("id", info.getId()).field("body", info.getBody());
        String json = xmlToJsonConverter.toJson(info.getBody());
        persistor.persist(json);
        LambdaResponse response = new OkLambdaResponse();
        response.setBody(json);
        logResponse(response, context);
        return response;
    }

    private void logEvent(SNSEvent snsEvent, Context context) {
        LOG.info()
                .message("snsEvent received")
                .field("snsEvent", snsEvent)
                .field("context", context)
                .log();
    }

    private void logResponse(LambdaResponse response, Context context) {
        LOG.info()
                .message("returning response")
                .field("response", response)
                .field("context", context)
                .log();
    }

}
