package uk.co.tpplc.middleware.receipt;

import uk.co.mruoc.http.client.HttpClient;
import uk.co.mruoc.http.client.Response;
import uk.co.mruoc.http.client.SimpleHttpClient;
import uk.co.mruoc.log.Logger;

public class ApiReceiptPersistor implements ReceiptPersistor {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(ApiReceiptPersistor.class);

    private final Environment environment;
    private final HttpClient httpClient;

    public ApiReceiptPersistor(Environment environment) {
        this(environment, new SimpleHttpClient());
    }

    public ApiReceiptPersistor(Environment environment, HttpClient httpClient) {
        this.environment = environment;
        this.httpClient = httpClient;
    }

    @Override
    public void persist(String json) {
        String url = environment.getPostReceiptUrl();
        LOG.info().message("calling api").field("url", url).field("method", "post").field("body", json).log();
        Response response = httpClient.post(url, json);
        LOG.info().message("api response").field("statusCode", response.getStatusCode()).field("body", response.getBody()).log();
    }

}
