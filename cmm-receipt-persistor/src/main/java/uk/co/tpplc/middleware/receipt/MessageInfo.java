package uk.co.tpplc.middleware.receipt;

public class MessageInfo {

    private final String id;
    private final String body;

    public MessageInfo(String id, String body) {
        this.id = id;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

}
