package uk.co.tpplc.middleware.receipt;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ReceiptJsonParser {

    public String parseId(String input) {
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(input);
        return element.getAsJsonObject()
                .getAsJsonObject("DeliveryReceipt")
                .getAsJsonObject("DeliveryReceiptLine")
                .getAsJsonObject("PurchaseOrder")
                .getAsJsonObject("PurchaseOrderIds")
                .get("Id")
                .getAsString();
    }

}
