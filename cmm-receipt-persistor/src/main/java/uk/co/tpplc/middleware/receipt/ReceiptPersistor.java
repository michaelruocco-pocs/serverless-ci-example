package uk.co.tpplc.middleware.receipt;

public interface ReceiptPersistor {

    void persist(String json);

}
