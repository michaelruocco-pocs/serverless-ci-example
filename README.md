# Serverless CI Example

## Setup

Before you start you will need to install npm and version 1.18.0
of the serverless framework.

To do this you will need to install version 7.10.0 of nodejs, and version 4.2.0
of npm. If you are running the WSO2 engineering VM then you can do this by running
the following commands

```
sudo apt-get update
sudo curl -sL https://deb.nodesource.com/setup_7.x | bash -
sudo apt-get install -y nodejs
```

Once this has completed you can check your versions by running:

```
node -v (should show 7.10.0)
npm -v (should show 4.2.0)
```

Once that is complete you can install the serverless framework by running:

```
sudo npm install serverless@1.18.0 -g
```

Once this has completed you can check the version by running:

```
sls -v (should show 1.18.0)
```

You are now in almost in a position where you can build and run the project in AWS from
your local machine. First you need to set up your environment to use java 8, on the
VM you should be able to do this by running the following alias command:

```
java8
```

Finally you will need to set up credentials to be able to point at an AWS account
to build the project resources into. To do this you will need to create a directory
under your home directory called .aws and then create a file inside called credentials
that contains your aws access key id and secret access key. The path to the file should be:

```
~/.aws/credentials
``` 

The content of the file should be:

```
[default]
aws_access_key_id=<access key id>
aws_secret_access_key=<access key value>
```

## Running the Unit tests

To build the project and run the unit tests:

```
./gradlew clean build
```

## Running the SIT Tests

To run the sit tests locally you can run the following:

```
./gradlew clean build -x test // builds the lambda zip file
sls deploy --stage sit // deploys required aws resources for sit testing
./gradlew sitTest //runs sitTests
sls remove --stage sit // removes aws resources after sit tests complete
```

If you are running in an shared account it is a good idea to append your username
to the stage value so that your tests won't clash with anyone else running tests in
the same account, an example of this is shown below:

```
./gradlew clean build -x test // builds the lambda zip file
sls deploy --stage sitmruoc // deploys required aws resources for sit testing
./gradlew sitTest -Dstage=sitmruoc //runs sitTests
sls remove --stage sitmruoc // removes aws resources after sit tests complete
```

When the CI process runs it is run using the stage sitci.

## Running tests behind a proxy

If you are working on a machine that is behind a proxy you need to provide
arguments to let the JVM know how to connect to the proxy server when trying
to get out to the internet, this means that the command to run the SIT tests
would become:

```
./gradlew sitTest -Dhttp.proxyHost=<host> -Dhttp.proxyPort=<port> -Dhttps.proxyHost=<host> -Dhttps.proxyPort=<port>
```

If you need to provide a stage value as well this can simply be added as
another parameter as required.

## Running tests from IDE

When running the unit tests for the common project, you need to configure
a Java system property to tell java where the sqllite4java native library
files are stored. This is handled for you when running the tests from gradle,
but if you want to run them from your IDE you will need to pass the following
argument when starting up the JVM:

```
-Dsqlite4java.library.path=./lib/sqlite4java
```

Likewise if you want to run the sit tests from your IDE you will need to pass
a stage value to match the stage value that you used when creating the AWS resources
by running serverless (note, the resource creation must be done from the prompt and
cannot be performed in the IDE.) To pass a stage value when running from the IDE
you will need to pass the stage as the example below shows:

```
-Dstage=sitmruoc
```

The proxy arguments will also need to be added as outlined above when running
from an IDE if your machine is behind a proxy server.

## Outstanding issues 

* modify ci pipeline so that lambda only needs to be compiled and built once
if possible 

* see if we can figure out why tests don't seem to be using region environment
variable when clients are created, currently this is having to be explicitly set

* change iamRoles definition so each lambda function get its own role and policies

* handle test dependencies to so that test classes can be refactored into common
project to remove duplicated test classes, e.g. TestLines, FakeContext and
FakeDynamoTableClient.

