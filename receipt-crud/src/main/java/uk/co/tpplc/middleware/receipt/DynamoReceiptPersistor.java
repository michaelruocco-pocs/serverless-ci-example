package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import uk.co.mruoc.dynamo.SimpleTable;
import uk.co.mruoc.dynamo.TableConfig;
import uk.co.mruoc.log.Logger;

public class DynamoReceiptPersistor implements ReceiptPersistor {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(DynamoReceiptPersistor.class);

    private final ReceiptJsonParser receiptJsonParser = new ReceiptJsonParser();
    private final TableConfig tableConfig;
    private final SimpleTable table;

    public DynamoReceiptPersistor(Environment environment) {
        this(CmmReceiptTableFactory.buildConfig(environment), CmmReceiptTableFactory.buildTable(LOG));
    }

    public DynamoReceiptPersistor(TableConfig tableConfig, SimpleTable table) {
        this.tableConfig = tableConfig;
        this.table = table;
    }

    @Override
    public void persist(String json) {
        String id = receiptJsonParser.parseId(json);
        logInfo("extracted receipt id " + id);

        Item item = buildItem(id, json);
        logWriteItem(item);
        table.write(tableConfig, item);
    }

    private Item buildItem(String id, String json) {
        String idFieldName = tableConfig.getIdFieldName();
        return new Item()
                .withPrimaryKey(idFieldName, id)
                .withJSON("message", json);
    }

    private void logWriteItem(Item item) {
        LOG.info().message("writing item").field("item", item).log();
    }

    private void logInfo(String message) {
        LOG.info().message(message).log();
    }

}
