package uk.co.tpplc.middleware.receipt;

import org.json.JSONObject;

public class ReceiptJsonParser {

    public String parseId(String input) {
        JSONObject json = new JSONObject(input);
        return json.getJSONObject("DeliveryReceipt")
                .getJSONObject("DeliveryReceiptLine")
                .getJSONObject("PurchaseOrder")
                .getJSONObject("PurchaseOrderIds")
                .getString("Id");
    }

}
