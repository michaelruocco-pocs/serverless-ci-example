package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import uk.co.mruoc.lambda.*;
import uk.co.mruoc.log.Logger;

import java.util.Optional;

public class GetReceiptHandler implements RequestHandler<ApiGatewayRequest, LambdaResponse> {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(GetReceiptHandler.class);

    private final ReceiptLoader receiptLoader;

    public GetReceiptHandler() {
        this(new DefaultEnvironment());
    }

    public GetReceiptHandler(Environment environment) {
        this(new DynamoReceiptLoader(environment));
    }

    public GetReceiptHandler(ReceiptLoader receiptLoader) {
        this.receiptLoader = receiptLoader;
    }

    @Override
    public LambdaResponse handleRequest(ApiGatewayRequest request, Context context) {
        logRequest(request, context);
        String id = request.getId();
        Optional<Item> item = receiptLoader.load(id);
        LambdaResponse response = buildResponse(id, item);
        logResponse(response, context);
        return response;
    }

    private void logRequest(ApiGatewayRequest request, Context context) {
        LOG.info()
                .message("api gateway request received")
                .field("apiGatewayRequest", request)
                .field("context", context)
                .log();
    }

    private LambdaResponse buildResponse(String id, Optional<Item> item) {
        if (item.isPresent())
            return buildOkResponse(item.get().toJSON());
        return buildNotFoundResponse(id);
    }

    private LambdaResponse buildNotFoundResponse(String id) {
        ErrorLambdaResponse response = new NotFoundLambdaResponse();
        response.setErrorMessage("receipt with id " + id + " not found");
        return response;
    }

    private LambdaResponse buildOkResponse(String body) {
        LambdaResponse response = new OkLambdaResponse();
        response.setBody(body);
        return response;
    }

    private void logResponse(LambdaResponse response, Context context) {
        LOG.info()
                .message("returning response")
                .field("response", response)
                .field("context", context)
                .log();
    }

}
