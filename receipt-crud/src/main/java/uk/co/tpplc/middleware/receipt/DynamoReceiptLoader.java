package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import uk.co.mruoc.dynamo.Items;
import uk.co.mruoc.dynamo.SimpleTable;
import uk.co.mruoc.dynamo.TableConfig;
import uk.co.mruoc.log.Logger;

import java.util.Optional;

public class DynamoReceiptLoader implements ReceiptLoader {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(DynamoReceiptLoader.class);

    private final TableConfig tableConfig;
    private final SimpleTable table;

    public DynamoReceiptLoader(Environment environment) {
        this(CmmReceiptTableFactory.buildConfig(environment), CmmReceiptTableFactory.buildTable(LOG));
    }

    public DynamoReceiptLoader(TableConfig tableConfig, SimpleTable table) {
        this.tableConfig = tableConfig;
        this.table = table;
    }

    @Override
    public Optional<Item> load(String id) {
        LOG.info().message("loading receipt").field("id", id).field("tableConfig", tableConfig).log();
        PrimaryKey key = new PrimaryKey(tableConfig.getIdFieldName(), id);
        Item item = table.get(tableConfig, key);
        if (item == null) {
            LOG.info().message("receipt not found").field("id", id).field("tableConfig", tableConfig).log();
            return Optional.empty();
        }
        LOG.info().message("loaded receipt").field("receipt", item).log();
        return Optional.of(item);
    }

    @Override
    public Items loadAll() {
        LOG.info().message("loading all receipts").field("tableConfig", tableConfig).log();
        Items items = table.getAll(tableConfig);
        LOG.info().message("loaded " + items.getCount() + " receipts").field("hasMorePages", items.hasMorePages()).field("lastEvaluatedKey", items.getLastEvaluatedKey()).log();
        return items;
    }

    @Override
    public Items loadAll(String lastEvaluatedKey) {
        LOG.info().message("loading all receipts").field("tableConfig", tableConfig).field("lastEvaluatedKey", lastEvaluatedKey).log();
        Items items = table.getAll(tableConfig, lastEvaluatedKey);
        LOG.info().message("loaded " + items.getCount() + " receipts").field("hasMorePages", items.hasMorePages()).field("lastEvaluatedKey", items.getLastEvaluatedKey()).log();
        return items;
    }

}
