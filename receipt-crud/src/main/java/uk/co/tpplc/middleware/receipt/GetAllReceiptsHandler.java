package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import uk.co.mruoc.dynamo.Items;
import uk.co.mruoc.lambda.ApiGatewayRequest;
import uk.co.mruoc.lambda.LambdaResponse;
import uk.co.mruoc.lambda.OkLambdaResponse;
import uk.co.mruoc.log.Logger;

public class GetAllReceiptsHandler implements RequestHandler<ApiGatewayRequest, LambdaResponse> {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(GetAllReceiptsHandler.class);

    private final ReceiptLoader receiptLoader;

    public GetAllReceiptsHandler() {
        this(new DynamoReceiptLoader(new DefaultEnvironment()));
    }

    public GetAllReceiptsHandler(ReceiptLoader receiptLoader) {
        this.receiptLoader = receiptLoader;
    }

    @Override
    public LambdaResponse handleRequest(ApiGatewayRequest request, Context context) {
        logRequest(request, context);
        Items items = loadAllItems(request);
        LambdaResponse response = buildResponse(items);
        logResponse(response, context);
        return response;
    }

    private Items loadAllItems(ApiGatewayRequest request) {
        if (request.hasHeader("last-evaluated-key"))
            return receiptLoader.loadAll(request.getHeader("last-evaluated-key"));
        return receiptLoader.loadAll();
    }

    private LambdaResponse buildResponse(Items items) {
        LambdaResponse response = new OkLambdaResponse();
        response.setBody(items.toJsonArray());
        if (items.hasMorePages())
            response.addHeader("last-evaluated-key", items.getLastEvaluatedKey());
        return response;
    }

    private void logRequest(ApiGatewayRequest request, Context context) {
        LOG.info()
                .message("api gateway request received")
                .field("apiGatewayRequest", request)
                .field("context", context)
                .log();
    }

    private void logResponse(LambdaResponse response, Context context) {
        LOG.info()
                .message("returning response")
                .field("response", response)
                .field("context", context)
                .log();
    }
}
