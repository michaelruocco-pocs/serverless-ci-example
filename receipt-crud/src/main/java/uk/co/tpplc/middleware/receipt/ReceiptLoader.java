package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import uk.co.mruoc.dynamo.Items;

import java.util.Optional;

public interface ReceiptLoader {

    Optional<Item> load(String id);

    Items loadAll();

    Items loadAll(String lastEvaluatedKey);

}
