package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import uk.co.mruoc.lambda.*;
import uk.co.mruoc.log.Logger;

public class PostReceiptHandler implements RequestHandler<ApiGatewayRequest, LambdaResponse> {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(PostReceiptHandler.class);

    private final ReceiptPersistor receiptPersistor;

    public PostReceiptHandler() {
        this(new DefaultEnvironment());
    }

    public PostReceiptHandler(Environment environment) {
        this(new DynamoReceiptPersistor(environment));
    }

    public PostReceiptHandler(ReceiptPersistor receiptPersistor) {
        this.receiptPersistor = receiptPersistor;
    }

    @Override
    public LambdaResponse handleRequest(ApiGatewayRequest request, Context context) {
        logRequest(request, context);
        String json = request.getBody();
        receiptPersistor.persist(json);
        LambdaResponse response = buildCreatedResponse(json);
        logResponse(response, context);
        return response;
    }

    private void logRequest(ApiGatewayRequest request, Context context) {
        LOG.info()
                .message("api gateway request received")
                .field("apiGatewayRequest", request)
                .field("context", context)
                .log();
    }

    private LambdaResponse buildCreatedResponse(String body) {
        LambdaResponse response = new CreatedLambdaResponse();
        response.setBody(body);
        return response;
    }

    private void logResponse(LambdaResponse response, Context context) {
        LOG.info()
                .message("returning response")
                .field("response", response)
                .field("context", context)
                .log();
    }

}
