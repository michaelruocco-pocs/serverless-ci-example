package uk.co.tpplc.middleware.receipt;

public class TestLines {

    public static final String VALID_RECEIPT_LINE_JSON = "{\"DeliveryReceipt\":" +
            "{\"ActualDeliveryDateTime\":\"16-02-2017\"," +
            "\"DeliveryReceiptLine\":" +
            "{\"ReceivedQuantity\":220," +
            "\"PurchaseOrder\":" +
            "{\"PurchaseOrderIds\":" +
            "{\"Id\":\"0462831111\"," +
            "\"IdType\":\"T\"}," +
            "\"Supplier\":{\"Id\":\"SM8896\"}}," +
            "\"PurchaseOrderLineItem\":{\"LineNumber\":1}}," +
            "\"ExternalGRN\":\"MS422074\"," +
            "\"DocumentDateTime\":\"15-02-2017\"}}";

}
