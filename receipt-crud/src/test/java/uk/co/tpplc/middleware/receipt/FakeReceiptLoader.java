package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import org.apache.commons.lang3.StringUtils;
import uk.co.mruoc.dynamo.Items;
import uk.co.mruoc.dynamo.Items.ItemsBuilder;

import java.util.*;

public class FakeReceiptLoader implements ReceiptLoader {

    private final Map<String, Item> receipts = new HashMap<>();
    private String lastEvaluatedKey = StringUtils.EMPTY;
    private boolean hasMorePages = false;

    @Override
    public Optional<Item> load(String id) {
        if (receipts.containsKey(id))
            return Optional.of(receipts.get(id));
        return Optional.empty();
    }

    @Override
    public Items loadAll() {
        return new ItemsBuilder()
                .setItems(new ArrayList<>(receipts.values()))
                .setLastEvaluatedKey(lastEvaluatedKey)
                .build();
    }

    @Override
    public Items loadAll(String lastEvaluatedKey) {
        this.lastEvaluatedKey = lastEvaluatedKey;
        return loadAll();
    }

    public void addReceipt(Item item) {
        receipts.put(item.getString("id"), item);
    }

    public void setLastEvaluatedKey(String lastEvaluatedKey) {
        this.lastEvaluatedKey = lastEvaluatedKey;
    }

    public String getLastEvaluatedKey() {
        return lastEvaluatedKey;
    }
}
