package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import org.junit.Before;
import org.junit.Test;
import uk.co.mruoc.dynamo.FakeItem;
import uk.co.mruoc.dynamo.FakeSimpleTable;
import uk.co.mruoc.dynamo.Items;
import uk.co.mruoc.dynamo.TableConfig;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class DynamoReceiptLoaderTest {

    private static final String ID_FIELD_NAME = "id";

    private final TableConfig tableConfig = mock(TableConfig.class);
    private final FakeSimpleTable table = new FakeSimpleTable();

    private final DynamoReceiptLoader loader = new DynamoReceiptLoader(tableConfig, table);

    @Before
    public void setUp() {
        given(tableConfig.getIdFieldName()).willReturn(ID_FIELD_NAME);
    }

    @Test
    public void shouldUseTableConfigWhenLoadingSingleReceiptFromDynamo() {
        String id = "id-value";
        Item item = new FakeItem(id, "{}");
        givenTableClientWillReturnReceipt(item);

        loader.load(id);

        assertThat(table.getLastWrittenTableConfig()).isEqualTo(tableConfig);
    }

    @Test
    public void shouldReturnSingleReceiptJsonFromDynamo() {
        String id = "id-value";
        Item expectedItem = new FakeItem(id, "{\"some\":\"value\"}");
        givenTableClientWillReturnReceipt(expectedItem);

        Optional<Item> item = loader.load(id);

        assertThat(item.get()).isEqualTo(expectedItem);
    }

    @Test
    public void shouldUseTableConfigWhenLoadingAllReceiptsFromDynamo() {
        Item item1 = new FakeItem("1", "{\"some\":\"value1\"}");
        Item item2 = new FakeItem("2", "{\"some\":\"value2\"}");
        givenTableClientWillReturnAllReceipts(item1, item2);

        loader.loadAll();

        assertThat(table.getLastReadTableConfig()).isEqualTo(tableConfig);
    }

    @Test
    public void shouldReturnAllReceiptsFromDynamo() {
        Item item1 = new FakeItem("1", "{\"some\":\"value1\"}");
        Item item2 = new FakeItem("2", "{\"some\":\"value2\"}");
        givenTableClientWillReturnAllReceipts(item1, item2);

        Items items = loader.loadAll();

        assertThat(items).containsExactly(item1, item2);
    }

    @Test
    public void shouldReturnAllReceiptsFromDynamoWithLastUsedKey() {
        String startKey = "1";
        Item item1 = new FakeItem("1", "{\"some\":\"value1\"}");
        Item item2 = new FakeItem("2", "{\"some\":\"value2\"}");
        givenTableClientWillReturnAllReceipts(item1, item2);

        Items items = loader.loadAll(startKey);

        assertThat(items).containsExactly(item1, item2);
        assertThat(table.getLastStartKey()).isEqualTo(startKey);
    }

    private void givenTableClientWillReturnReceipt(Item item) {
        table.setItemToReturnForId(item.getString("id"), item);
    }

    private void givenTableClientWillReturnAllReceipts(Item... items) {
        table.setItemsToReturnForAll(items);
    }

}
