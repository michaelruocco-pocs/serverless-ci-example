package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import org.junit.Before;
import org.junit.Test;
import uk.co.mruoc.dynamo.FakeSimpleTable;
import uk.co.mruoc.dynamo.TableConfig;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static uk.co.tpplc.middleware.receipt.TestLines.VALID_RECEIPT_LINE_JSON;

public class DynamoReceiptPersistorTest {

    private static final String ID_FIELD_NAME = "id";

    private final TableConfig tableConfig = mock(TableConfig.class);
    private final FakeSimpleTable table = new FakeSimpleTable();

    private final DynamoReceiptPersistor persistor = new DynamoReceiptPersistor(tableConfig, table);

    @Before
    public void setUp() {
        given(tableConfig.getIdFieldName()).willReturn(ID_FIELD_NAME);
    }

    @Test
    public void shouldUseTableConfigWhenWritingToDynamo() {
        persistor.persist(VALID_RECEIPT_LINE_JSON);

        assertThat(table.getLastWrittenTableConfig()).isEqualTo(tableConfig);
    }

    @Test
    public void shouldWriteItemJsonAsMessage() {
        persistor.persist(VALID_RECEIPT_LINE_JSON);

        Item item = table.getLastWrittenItem();
        assertThat(item.getJSON("message")).isEqualTo(VALID_RECEIPT_LINE_JSON);
    }

    @Test
    public void shouldWriteItemWithReceiptIdAsId() {
        persistor.persist(VALID_RECEIPT_LINE_JSON);

        Item item = table.getLastWrittenItem();
        assertThat(item.get(ID_FIELD_NAME)).isEqualTo("0462831111");
    }

}
