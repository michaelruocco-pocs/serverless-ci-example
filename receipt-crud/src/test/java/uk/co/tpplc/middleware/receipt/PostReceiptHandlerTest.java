package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.Context;
import org.junit.Test;
import uk.co.mruoc.lambda.ApiGatewayRequest;
import uk.co.mruoc.lambda.LambdaResponse;

import static org.assertj.core.api.Assertions.assertThat;

public class PostReceiptHandlerTest {

    private final FakeReceiptPersistor receiptPersistor = new FakeReceiptPersistor();
    private final Context context = new FakeContext();

    private final PostReceiptHandler handler = new PostReceiptHandler(receiptPersistor);

    @Test
    public void shouldReturn201ResponseOnSuccess() {
        String body = "my-body";
        ApiGatewayRequest request = buildRequestWithBody(body);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getStatusCode()).isEqualTo(201);
    }

    @Test
    public void shouldReturnReceiptJsonInResponseBodyOnSuccess() {
        String body = "my-body";
        ApiGatewayRequest request = buildRequestWithBody(body);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getBody()).isEqualTo(body);
    }

    @Test
    public void shouldPersistBody() {
        String body = "my-body";
        ApiGatewayRequest request = buildRequestWithBody(body);

        handler.handleRequest(request, context);

        assertThat(receiptPersistor.getLastPersistedJson()).isEqualTo(body);
    }

    private ApiGatewayRequest buildRequestWithBody(String body) {
        ApiGatewayRequest request = new ApiGatewayRequest();
        request.setBody(body);
        return request;
    }

}
