package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.lambda.runtime.Context;
import org.junit.Test;
import uk.co.mruoc.dynamo.FakeItem;
import uk.co.mruoc.lambda.ApiGatewayRequest;
import uk.co.mruoc.lambda.LambdaResponse;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class GetAllReceiptsHandlerTest {

    private final FakeReceiptLoader receiptLoader = new FakeReceiptLoader();
    private final ApiGatewayRequest request = new ApiGatewayRequest();
    private final Context context = new FakeContext();

    private final GetAllReceiptsHandler handler = new GetAllReceiptsHandler(receiptLoader);

    @Test
    public void shouldReturn200ResponseOnSuccess() {
        Item item = new FakeItem("id-value","{ \"some\": \"value1\" }");
        receiptLoader.addReceipt(item);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    public void shouldReturnReceiptJsonInResponseBodyOnSuccess() {
        Item receipt1 = new FakeItem("id-value1", "{ \"some\": \"value1\" }");
        receiptLoader.addReceipt(receipt1);

        Item receipt2 = new FakeItem("id-value2", "{ \"some\": \"value2\" }");
        receiptLoader.addReceipt(receipt2);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getBody()).contains(receipt1.toJSON());
        assertThat(response.getBody()).contains(receipt2.toJSON());
    }

    @Test
    public void shouldSetLastEvaluatedKeyOnReceiptLoader() {
        String lastEvaluatedKey = "abcd";
        Map<String, String> headers = new HashMap<>();
        headers.put("last-evaluated-key", lastEvaluatedKey);
        request.setHeaders(headers);

        handler.handleRequest(request, context);

        assertThat(receiptLoader.getLastEvaluatedKey()).isEqualTo(lastEvaluatedKey);
    }

    @Test
    public void shouldReturnLastEvaluatedKeyIfReceiptLoaderHasMorePages() {
        String lastEvaluatedKey = "abcdef";
        receiptLoader.setLastEvaluatedKey(lastEvaluatedKey);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getHeader("last-evaluated-key")).isEqualTo(lastEvaluatedKey);
    }

}
