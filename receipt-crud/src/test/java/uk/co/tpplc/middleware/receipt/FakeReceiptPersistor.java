package uk.co.tpplc.middleware.receipt;

public class FakeReceiptPersistor implements ReceiptPersistor {

    private String lastPersistedJson;

    @Override
    public void persist(String json) {
        this.lastPersistedJson = json;
    }

    public String getLastPersistedJson() {
        return lastPersistedJson;
    }

}
