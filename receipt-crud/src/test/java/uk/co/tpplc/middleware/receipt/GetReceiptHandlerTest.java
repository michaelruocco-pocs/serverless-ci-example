package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.lambda.runtime.Context;
import org.junit.Test;
import uk.co.mruoc.dynamo.FakeItem;
import uk.co.mruoc.lambda.ApiGatewayRequest;
import uk.co.mruoc.lambda.LambdaResponse;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class GetReceiptHandlerTest {

    private final FakeReceiptLoader receiptLoader = new FakeReceiptLoader();
    private final Context context = new FakeContext();

    private final GetReceiptHandler handler = new GetReceiptHandler(receiptLoader);

    @Test
    public void shouldReturn200ResponseOnSuccess() {
        String id = "id-value";
        Item item = new FakeItem(id, "{ \"some\": \"value\" }");
        receiptLoader.addReceipt(item);
        ApiGatewayRequest request = buildRequestWithId(id);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    public void shouldReturnReceiptJsonInResponseBodyOnSuccess() {
        String id = "id-value";
        Item item = new FakeItem(id, "{ \"some\": \"value\" }");
        receiptLoader.addReceipt(item);
        ApiGatewayRequest request = buildRequestWithId(id);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getBody()).isEqualTo(item.toJSON());
    }

    @Test
    public void shouldReturn404ResponseIfReceiptNotFound() {
        String id = "id-value";
        ApiGatewayRequest request = buildRequestWithId(id);

        LambdaResponse response = handler.handleRequest(request, context);

        assertThat(response.getStatusCode()).isEqualTo(404);
    }

    private ApiGatewayRequest buildRequestWithId(String id) {
        ApiGatewayRequest request = new ApiGatewayRequest();
        Map<String, String> pathParameters = new HashMap<>();
        pathParameters.put("id", id);
        request.setPathParameters(pathParameters);
        return request;
    }

}
