package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishResult;
import org.junit.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class SnsTopicClientTest {

    private final AmazonSNS sns = mock(AmazonSNS.class);

    private final SnsTopicClient client = new SnsTopicClient(sns);

    @Test
    public void shouldWriteMessageToTopic() {
        String topicArn = "aws:topic:arn";
        String message = "this is a message";
        given(sns.publish(topicArn, message)).willReturn(new PublishResult());

        client.write(topicArn, message);
    }

}
