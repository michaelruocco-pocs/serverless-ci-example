package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.co.tpplc.middleware.receipt.TestLines.VALID_RECEIPT_LINE;
import static uk.co.tpplc.middleware.receipt.TestLines.VALID_RECEIPT_LINE_XML;

public class ReceiptLinePublisherTest {

    private static final String TOPIC_ARN = "topic:arn";

    private final FakeEnvironment environment = new FakeEnvironment();
    private final FakeTopicClient topicClient = new FakeTopicClient();

    private final ReceiptLinePublisher publisher = new ReceiptLinePublisher(environment, topicClient);

    @Test
    public void shouldWriteConvertedLinesToTopic() {
        List<ReceiptLine> lines = buildReceiptLines();
        mockTopicArn(TOPIC_ARN);

        publisher.publish(lines);

        List<String> messages = topicClient.getMessages(TOPIC_ARN);
        assertThat(messages.size()).isEqualTo(lines.size());
        assertThat(messages.get(0)).isEqualToIgnoringWhitespace(VALID_RECEIPT_LINE_XML);
        assertThat(messages.get(1)).isEqualToIgnoringWhitespace(VALID_RECEIPT_LINE_XML);
    }

    private List<ReceiptLine> buildReceiptLines() {
        return Arrays.asList(VALID_RECEIPT_LINE, VALID_RECEIPT_LINE);
    }

    private void mockTopicArn(String topicArn) {
        environment.setCmmReceiptTopicArn(topicArn);
    }

}
