package uk.co.tpplc.middleware.receipt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FakeTopicClient implements TopicClient {

    private Map<String, List<String>> messages = new HashMap<>();

    @Override
    public void write(String topicArn, String message) {
        List<String> topicMessages = getMessages(topicArn);
        topicMessages.add(message);
    }

    public List<String> getMessages(String topicArn) {
        if (messages.containsKey(topicArn))
            return messages.get(topicArn);

        List<String> topicMessages = new ArrayList<>();
        messages.put(topicArn, topicMessages);
        return topicMessages;
    }

}
