package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.tpplc.middleware.receipt.LineCountFormatter;

import static org.assertj.core.api.Assertions.assertThat;

public class LineCountFormatterTest {

    @Test
    public void shouldFormatSingleIntegerValue() {
        int lineCount = 2;

        String result = LineCountFormatter.format(lineCount);

        assertThat(result).isEqualTo("0000000002");
    }

    @Test
    public void shouldFormatDoubleIntegerValue() {
        int lineCount = 22;

        String result = LineCountFormatter.format(lineCount);

        assertThat(result).isEqualTo("0000000022");
    }

    @Test
    public void shouldFormatTripleIntegerValue() {
        int lineCount = 222;

        String result = LineCountFormatter.format(lineCount);

        assertThat(result).isEqualTo("0000000222");
    }

}
