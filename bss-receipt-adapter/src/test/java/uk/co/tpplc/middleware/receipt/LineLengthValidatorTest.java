package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.tpplc.middleware.receipt.LineLengthValidator;

import static org.assertj.core.api.Assertions.assertThat;

public class LineLengthValidatorTest {

    @Test
    public void shouldReturnTrueIfNumberOfDelimitedItemsMatchesSize() {
        int size = 3;
        LineLengthValidator validator = new LineLengthValidator(size);

        boolean valid = validator.isValid(" ~ ~ ");

        assertThat(valid).isTrue();
    }

    @Test
    public void shouldReturnFalseIfNumberOfDelimitedItemsDoesNotMatchesSize() {
        int size = 3;
        LineLengthValidator validator = new LineLengthValidator(size);

        boolean valid = validator.isValid(" ");

        assertThat(valid).isFalse();
    }

}
