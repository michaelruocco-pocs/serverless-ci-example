package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LegacyReceiptLineFilterTest {

    private final ReceiptLine legacyLine1 = TestLines.VALID_LEGACY_RECEIPT_LINE;
    private final ReceiptLine legacyLine2 = TestLines.VALID_LEGACY_RECEIPT_LINE;
    private final ReceiptLine line1 = TestLines.VALID_RECEIPT_LINE;
    private final ReceiptLine line2 = TestLines.VALID_RECEIPT_LINE;

    private final List<ReceiptLine> lines = Arrays.asList(legacyLine1, legacyLine2, line1, line2);

    private final LegacyReceiptLineFilter filter = new LegacyReceiptLineFilter();

    @Test
    public void shouldFilterOutDefaultLines() {
        List<ReceiptLine> result = filter.filter(lines);
        assertThat(result).containsExactly(legacyLine1, legacyLine2);
    }

}
