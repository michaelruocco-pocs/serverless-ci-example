package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.tpplc.middleware.receipt.StringHandler;

import static org.assertj.core.api.Assertions.assertThat;

public class StringHandlerTest {

    private final StringHandler handler = new StringHandler();

    @Test
    public void shouldRemoveLeadingAndTrailingSpaces() {
        String value = " 123 ";
        assertThat(handler.cleanString(value)).isEqualTo("123");
    }

    @Test
    public void shouldRemoveLeadingZeros() {
        String value = " 0000123 ";
        assertThat(handler.cleanString(value)).isEqualTo("123");
    }

    @Test
    public void shouldReturnTrueIfValidNumber() {
        String value = " 000123.12 ";
        assertThat(handler.isValidNumber(value)).isTrue();
    }

    @Test
    public void shouldReturnFalseIfNotValidNumber() {
        String value = " value123.12 ";
        assertThat(handler.isValidNumber(value)).isFalse();
    }


}
