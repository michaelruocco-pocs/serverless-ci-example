package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.tpplc.middleware.receipt.LineSplitter;

import static org.assertj.core.api.Assertions.assertThat;

public class LineSplitterTest {

    private final LineSplitter splitter = new LineSplitter();

    @Test
    public void shouldSplitLineOnTilde() {
        String[] data = splitter.split(" ~ ");

        assertThat(data.length).isEqualTo(2);
        assertThat(data[0]).isEqualTo(" ");
        assertThat(data[1]).isEqualTo(" ");
    }

}
