package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.mruoc.s3.FakeSimpleS3;
import uk.co.mruoc.s3.ObjectDefinition;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class LegacyReceiptLinePublisherTest {

    private static final String BUCKET_NAME = "bss-legacy-receipts";
    private static final String OBJECT_KEY = "object-key";
    private static final String CONTENT = "converted content";

    private final FakeEnvironment environment = new FakeEnvironment();
    private final FakeSimpleS3 fileSystem = new FakeSimpleS3();
    private final LegacyReceiptLineConverter converter = mock(LegacyReceiptLineConverter.class);
    private final List<ReceiptLine> lines = new ArrayList<>();

    private final LegacyReceiptLinePublisher publisher = new LegacyReceiptLinePublisher(environment, fileSystem, converter);

    @Test
    public void shouldPublisherConvertedReceiptLines() {
        given(converter.toContent(lines)).willReturn(CONTENT);

        publisher.publish(OBJECT_KEY, lines);

        assertThat(fileSystem.getLastWrittenContent()).isEqualTo(CONTENT);
    }

    @Test
    public void shouldPublishToBssLegacyReceiptsBucketUsingObjectKey() {
        environment.setBssLegacyReceiptBucketName(BUCKET_NAME);

        publisher.publish(OBJECT_KEY, lines);

        ObjectDefinition objectDefinition = fileSystem.getLastWrittenObjectDefinition();
        assertThat(objectDefinition.getBucketName()).isEqualTo(BUCKET_NAME);
        assertThat(objectDefinition.getKey()).isEqualTo(OBJECT_KEY);
    }

}
