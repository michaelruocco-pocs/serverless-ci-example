package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static com.googlecode.catchexception.apis.BDDCatchException.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

public class TrailerLineValidatorTest {

    private final TrailerLineValidator validator = new TrailerLineValidator();

    @Test
    public void shouldReturnTrueIfTrailerIsValid() {
        boolean result = validator.validate("**EOF**~DATACOUNT~0000000008~ 000006842.60  ");

        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnThrowExceptionIfTrailerIsInvalidLength() {
        String trailer = "invalid~length~trailer";

        when(validator).validate(trailer);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid trailer: " + trailer + ": length should be 4");
    }

    @Test
    public void shouldReturnThrowExceptionIfFirstTrailerEntryIsNotEof() {
        String trailer = "invalid~trailer~1~2";

        when(validator).validate(trailer);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid trailer: " + trailer + ": first entry should be **EOF**");
    }

    @Test
    public void shouldReturnThrowExceptionIfSecondTrailerEntryIsNotDataCount() {
        String trailer = "**EOF**~trailer~1~2";

        when(validator).validate(trailer);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid trailer: " + trailer + ": second entry should be DATACOUNT");
    }

    @Test
    public void shouldReturnThrowExceptionIfThirdTrailerEntryIsNotNumeric() {
        String trailer = "**EOF**~DATACOUNT~value1~2";

        when(validator).validate(trailer);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid trailer: " + trailer + ": line count should be numeric");
    }

    @Test
    public void shouldReturnThrowExceptionIfFourthTrailerEntryIsNotNumeric() {
        String trailer = "**EOF**~DATACOUNT~ 0001 ~value2";

        when(validator).validate(trailer);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid trailer: " + trailer + ": total value should be numeric");
    }

}
