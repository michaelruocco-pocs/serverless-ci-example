package uk.co.tpplc.middleware.receipt;

import org.assertj.core.api.StringAssert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ReceiptLineConverterTest {

    private final ReceiptLineConverter converter = new ReceiptLineConverter();

    @Test
    public void shouldConvertToXml() {
        String xml = converter.toXml(TestLines.VALID_RECEIPT_LINE);

        assertThat(xml).isEqualToIgnoringWhitespace(TestLines.VALID_RECEIPT_LINE_XML);
    }

}
