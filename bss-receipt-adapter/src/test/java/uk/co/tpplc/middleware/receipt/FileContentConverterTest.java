package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.mruoc.properties.ClasspathFileContentLoader;
import uk.co.mruoc.properties.FileContentLoader;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FileContentConverterTest {

    private static final String PATH = "/bss-receipt-valid.txt";

    private final FileContentLoader contentLoader = new ClasspathFileContentLoader();
    private final String content = contentLoader.loadContent(PATH);

    private final FileContentConverter converter = new FileContentConverter();

    @Test
    public void shouldReturnReceiptLinesAndRemoveTrailerLine() {
        List<ReceiptLine> lines = converter.toReceiptLines(content);

        assertThat(lines.size()).isEqualTo(8);
    }

}
