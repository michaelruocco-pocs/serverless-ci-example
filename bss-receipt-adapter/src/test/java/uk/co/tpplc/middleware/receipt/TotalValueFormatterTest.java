package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.tpplc.middleware.receipt.TotalValueFormatter;

import static org.assertj.core.api.Assertions.assertThat;

public class TotalValueFormatterTest {

    @Test
    public void shouldFormatSingleIntegerValue() {
        double value = 5;

        String result = TotalValueFormatter.format(value);

        assertThat(result).isEqualTo(" 000000005.00 ");
    }

    @Test
    public void shouldFormatDoubleIntegerValue() {
        double value = 25;

        String result = TotalValueFormatter.format(value);

        assertThat(result).isEqualTo(" 000000025.00 ");
    }

    @Test
    public void shouldFormatTripleIntegerValue() {
        double value = 125;

        String result = TotalValueFormatter.format(value);

        assertThat(result).isEqualTo(" 000000125.00 ");
    }

    @Test
    public void shouldFormatSingleDecimalValue() {
        double value = 25.4;

        String result = TotalValueFormatter.format(value);

        assertThat(result).isEqualTo(" 000000025.40 ");
    }

    @Test
    public void shouldFormatDoubleDecimalValue() {
        double value = 25.44;

        String result = TotalValueFormatter.format(value);

        assertThat(result).isEqualTo(" 000000025.44 ");
    }

}
