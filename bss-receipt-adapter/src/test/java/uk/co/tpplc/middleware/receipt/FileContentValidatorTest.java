package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.mruoc.properties.ClasspathFileContentLoader;
import uk.co.mruoc.properties.FileContentLoader;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static com.googlecode.catchexception.apis.BDDCatchException.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

public class FileContentValidatorTest {

    private final FileContentValidator validator = new FileContentValidator();

    @Test
    public void shouldReturnTrueIfContentIsValid() {
        String validContentPath = "/bss-receipt-valid.txt";
        String content = loadContent(validContentPath);

        boolean result = validator.validate(content);

        assertThat(result).isTrue();
    }

    @Test
    public void shouldThrowExceptionIfLineCountIsNotValid() {
        String validContentPath = "/bss-receipt-invalid-line-count.txt";
        String content = loadContent(validContentPath);

        when(validator).validate(content);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid file: number of lines 8 trailer has 7");
    }

    @Test
    public void shouldThrowExceptionIfTotalValueIsNotValid() {
        String validContentPath = "/bss-receipt-invalid-total-value.txt";
        String content = loadContent(validContentPath);

        when(validator).validate(content);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid file: total value 6842.6 trailer has 5842.6");
    }

    private static String loadContent(String path) {
        FileContentLoader contentLoader = new ClasspathFileContentLoader();
        return contentLoader.loadContent(path);
    }

}
