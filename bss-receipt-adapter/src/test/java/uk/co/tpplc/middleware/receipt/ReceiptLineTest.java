package uk.co.tpplc.middleware.receipt;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.co.tpplc.middleware.receipt.ReceiptLine.*;

public class ReceiptLineTest {

    private final ReceiptLineBuilder builder = new ReceiptLineBuilder();

    @Test
    public void shouldReturnOriginalLine() {
        String originalLine = "originalLine";
        builder.setOriginalLine(originalLine);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getOriginalLine()).isEqualTo(originalLine);
    }

    @Test
    public void shouldReturnPurchaseOrderId() {
        String purchaseOrderId = "0462831111";
        builder.setPurchaseOrderId(purchaseOrderId);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getPurchaseOrderId()).isEqualTo(purchaseOrderId);
    }

    @Test
    public void isLegacyShouldReturnTruePurchaseOrderIdIsSixDigits() {
        String purchaseOrderId = "123456";
        builder.setPurchaseOrderId(purchaseOrderId);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.isLegacy()).isTrue();
    }

    @Test
    public void isLegacyShouldReturnFalseIfPurchaseOrderIdLongerThanSixDigits() {
        String purchaseOrderId = "1234567";
        builder.setPurchaseOrderId(purchaseOrderId);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.isLegacy()).isFalse();
    }

    @Test
    public void isLegacyShouldReturnTrueIfPurchaseOrderIdShorterThanSixDigits() {
        String purchaseOrderId = "12345";
        builder.setPurchaseOrderId(purchaseOrderId);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.isLegacy()).isTrue();
    }

    @Test
    public void shouldReturnActualDeliveryDate() {
        DateTime actualDeliveryDate = DateTime.now();
        builder.setActualDeliveryDate(actualDeliveryDate);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getActualDeliveryDate()).isEqualTo(actualDeliveryDate);
    }

    @Test
    public void shouldReturnDocumentDate() {
        DateTime documentDate = DateTime.now();
        builder.setDocumentDate(documentDate);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getDocumentDate()).isEqualTo(documentDate);
    }

    @Test
    public void shouldReturnExternalGrn() {
        String externalGrn = "MS422074";
        builder.setExternalGrn(externalGrn);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getExternalGrn()).isEqualTo(externalGrn);
    }

    @Test
    public void shouldReturnReceivedQuantity() {
        double receivedQuantity = 220.0;
        builder.setReceivedQuantity(receivedQuantity);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getReceivedQuantity()).isEqualTo(220.0);
    }

    @Test
    public void shouldReturnSupplierId() {
        String supplierId = "SM8896";
        builder.setSupplierId(supplierId);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getSupplierId()).isEqualTo(supplierId);
    }

    @Test
    public void shouldReturnPurchaseOrderIdType() {
        String purchaseOrderIdType = "T";
        builder.setPurchaseOrderIdType(purchaseOrderIdType);

        ReceiptLine receiptLine = builder.build();

        assertThat(receiptLine.getPurchaseOrderIdType()).isEqualTo("T");
    }

}
