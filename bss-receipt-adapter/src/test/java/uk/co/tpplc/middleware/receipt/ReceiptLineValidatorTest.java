package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static com.googlecode.catchexception.apis.BDDCatchException.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

public class ReceiptLineValidatorTest {

    private final ReceiptLineValidator validator = new ReceiptLineValidator();

    @Test
    public void shouldReturnTrueIfLineIsValid() {
        boolean result = validator.validate(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnThrowExceptionIfLineIsInvalidLength() {
        String line = "invalid~length~line";

        when(validator).validate(line);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid receipt line: " + line + ": length should be 46");
    }

    @Test
    public void shouldReturnThrowExceptionIfEighthLineEntryIsNotNumeric() {
        String line = " ~ ~ ~ ~ ~ ~ ~val1~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ";

        when(validator).validate(line);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid receipt line: " + line + ": value should be numeric");
    }

}
