package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LegacyReceiptLineConverterTest {

    private static final String NEW_LINE = System.lineSeparator();
    private static final String TRAILER_DELIMITER = "~";
    private static final int TRAILER_LINE_COUNT_INDEX = 2;
    private static final int TRAILER_TOTAL_VALUE_INDEX = 3;

    private final ReceiptLine line1 = TestLines.VALID_LEGACY_RECEIPT_LINE;
    private final ReceiptLine line2 = TestLines.VALID_LEGACY_RECEIPT_LINE;
    private final List<ReceiptLine> lines = Arrays.asList(line1, line2);

    private final LegacyReceiptLineConverter converter = new LegacyReceiptLineConverter();

    @Test
    public void shouldAddTrailerLineWhenConvertingLinesToLegacyFileContent() {
        String content = converter.toContent(lines);

        String[] contentLines = content.split(NEW_LINE);

        assertThat(contentLines.length).isEqualTo(lines.size() + 1);
    }

    @Test
    public void shouldConvertAllReceiptLines() {
        String content = converter.toContent(lines);

        String[] contentLines = content.split(NEW_LINE);

        assertThat(contentLines[0]).isEqualTo(lines.get(0).getOriginalLine());
        assertThat(contentLines[1]).isEqualTo(lines.get(1).getOriginalLine());
    }

    @Test
    public void trailerShouldContainFormattedLineCount() {
        String content = converter.toContent(lines);

        String[] trailerData = getTrailerData(content);
        String lineCount = trailerData[TRAILER_LINE_COUNT_INDEX];
        assertThat(lineCount).isEqualTo("0000000002");
    }

    @Test
    public void trailerShouldContainFormattedTotalValue() {
        String content = converter.toContent(lines);

        String[] trailerData = getTrailerData(content);
        String totalValue = trailerData[TRAILER_TOTAL_VALUE_INDEX];
        assertThat(totalValue).isEqualTo(" 000002525.64 ");
    }

    private String[] getTrailerData(String content) {
        String[] contentLines = content.split(NEW_LINE);
        String trailer = contentLines[contentLines.length - 1];
        return trailer.split(TRAILER_DELIMITER);
    }

}
