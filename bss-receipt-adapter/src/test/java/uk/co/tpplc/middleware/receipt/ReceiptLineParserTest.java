package uk.co.tpplc.middleware.receipt;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ReceiptLineParserTest {

    private final ReceiptLineParser parser = new ReceiptLineParser();

    @Test
    public void shouldParseValue() {
        String value = parser.parseValue(TestLines.VALID_RECEIPT_LINE_TEXT);
        assertThat(value).isEqualTo("825.00");
    }

    @Test
    public void shouldParseValueToDouble() {
        double value = parser.parseValueToDouble(TestLines.VALID_RECEIPT_LINE_TEXT);
        assertThat(value).isEqualTo(825.00);
    }

    @Test
    public void shouldParsePurchaseOrderId() {
        ReceiptLine line = parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);
        assertThat(line.getPurchaseOrderId()).isEqualTo("0462831111");
    }

    @Test
    public void shouldParseActualDeliveryDate() {
        DateTime expectedDate = new DateTime(2017, 2, 16, 0, 0);

        ReceiptLine line = parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(line.getActualDeliveryDate()).isEqualTo(expectedDate);
    }

    @Test
    public void shouldParseDocumentDate() {
        DateTime expectedDate = new DateTime(2017, 2, 15, 0, 0);

        ReceiptLine line = parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(line.getDocumentDate()).isEqualTo(expectedDate);
    }

    @Test
    public void shouldParseExternalGrn() {
        ReceiptLine line = parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(line.getExternalGrn()).isEqualTo("MS422074");
    }

    @Test
    public void shouldParseReceivedQuantity() {
        ReceiptLine line =  parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(line.getReceivedQuantity()).isEqualTo(220.0);
    }

    @Test
    public void shouldParseSupplierId() {
        ReceiptLine line =  parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(line.getSupplierId()).isEqualTo("SM8896");
    }

    @Test
    public void shouldParsePurchaseOrderIdType() {
        ReceiptLine line = parser.parse(TestLines.VALID_RECEIPT_LINE_TEXT);

        assertThat(line.getPurchaseOrderIdType()).isEqualTo("T");
    }

}
