package uk.co.tpplc.middleware.receipt;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrailerLineParserTest {

    private static final String TRAILER = "**EOF**~DATACOUNT~0000000008~ 000006842.60  ";

    private final TrailerLineParser parser = new TrailerLineParser();

    @Test
    public void shouldParseEof() {
        String value = parser.parseEof(TRAILER);
        assertThat(value).isEqualTo("**EOF**");
    }

    @Test
    public void shouldParseDataCount() {
        String value = parser.parseDataCount(TRAILER);
        assertThat(value).isEqualTo("DATACOUNT");
    }

    @Test
    public void shouldParseLineCount() {
        String value = parser.parseLineCount(TRAILER);
        assertThat(value).isEqualTo("8");
    }

    @Test
    public void shouldParseLineCountToInt() {
        int value = parser.parseLineCountToInt(TRAILER);
        assertThat(value).isEqualTo(8);
    }

    @Test
    public void shouldParseTotalValue() {
        String value = parser.parseTotalValue(TRAILER);
        assertThat(value).isEqualTo("6842.60");
    }

    @Test
    public void shouldParseTotalValueToDouble() {
        double value = parser.parseTotalValueToDouble(TRAILER);
        assertThat(value).isEqualTo(6842.60);
    }

}
