package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import org.junit.Test;
import uk.co.mruoc.lambda.LambdaResponse;
import uk.co.mruoc.properties.ClasspathFileContentLoader;
import uk.co.mruoc.properties.FileContentLoader;
import uk.co.mruoc.s3.FakeSimpleS3;
import uk.co.mruoc.s3.ObjectDefinition;

import java.util.List;

import static com.googlecode.catchexception.apis.BDDCatchException.caughtException;
import static com.googlecode.catchexception.apis.BDDCatchException.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

public class HandlerTest {

    private static final String NEW_LINE = System.lineSeparator();
    private static final String PATH = "/bss-receipt-valid.txt";

    private final FakeS3EventBuilder s3EventBuilder = new FakeS3EventBuilder();
    private final Context context = new FakeContext();

    private final FileContentLoader contentLoader = new ClasspathFileContentLoader();
    private final String validContent = contentLoader.loadContent(PATH);
    private final FakeEnvironment environment = new FakeEnvironment();
    private final FakeSimpleS3 fileSystem = new FakeSimpleS3();
    private final FakeTopicClient topicWriter = new FakeTopicClient();

    private final Handler handler = new Handler(environment, fileSystem, topicWriter);

    @Test
    public void shouldReturn200StatusCodeInResponse() {
        fileSystem.setObjectAsString(validContent);
        S3Event event = s3EventBuilder.build();

        LambdaResponse response = handler.handleRequest(event, context);

        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    public void shouldReturnEmptyBodyInResponse() {
        fileSystem.setObjectAsString(validContent);
        S3Event event = s3EventBuilder.build();

        LambdaResponse response = handler.handleRequest(event, context);

        assertThat(response.getBody()).isEmpty();
    }

    @Test
    public void shouldThrowExceptionIdExpectedContentIsNotValid() {
        fileSystem.setObjectAsString("invalid content");
        String bucketName = "bss-legacy-receipts";
        environment.setBssLegacyReceiptBucketName(bucketName);
        S3Event event = s3EventBuilder.build();

        when(handler).handleRequest(event, context);

        then(caughtException())
                .isInstanceOf(ValidationException.class)
                .hasMessage("invalid trailer: invalid content: length should be 4");
    }

    @Test
    public void shouldWriteLegacyLinesWithTrailerToLegacyBucket() {
        fileSystem.setObjectAsString(validContent);
        String bucketName = "bss-legacy-receipts";
        environment.setBssLegacyReceiptBucketName(bucketName);
        String expectedContent = "11620418~A~1530~GBP~1~1" +
                "~                                                                      ~ 000000000001262.82" +
                "~446543348     ~20170215~00047769~carol~20170215~ 000000000001262.82~Y~ 000000000001262.82" +
                "~0000000000016.19~ 0000000000078.0000~        ~Y~11620418                ~    308888~T" +
                "~11620418                ~ 000000000001262.82~ 000000000001262.82~000000000016.19~ 0000000000078.0000" +
                "~1~1 ~1 ~108MM 29211 GALV PIPE MAPRESS CARBON    ~1GIS01  ~1~1~SG9057  ~R~M ~S~0000~0000~20170208~Y" +
                "~0000000000078.0000~Y~00022" + NEW_LINE + "**EOF**~DATACOUNT~0000000001~ 000001262.82 ";
        S3Event event = s3EventBuilder.build();

        handler.handleRequest(event, context);

        ObjectDefinition objectDefinition = fileSystem.getLastWrittenObjectDefinition();
        assertThat(objectDefinition.getBucketName()).isEqualTo(bucketName);
        assertThat(objectDefinition.getKey()).isEqualTo(event.getRecords().get(0).getS3().getObject().getKey());
        assertThat(fileSystem.getLastWrittenContent()).isEqualTo(expectedContent);
    }

    @Test
    public void shouldWriteReceiptLinesToCmmReceiptsQueue() {
        fileSystem.setObjectAsString(validContent);
        String topicArn = "topic:arn";
        environment.setCmmReceiptTopicArn(topicArn);

        S3Event event = s3EventBuilder.build();

        handler.handleRequest(event, context);

        List<String> messages = topicWriter.getMessages(topicArn);
        assertThat(messages.size()).isEqualTo(7);
        assertThat(messages.get(0)).isEqualToIgnoringWhitespace(TestLines.VALID_RECEIPT_LINE_XML);
    }

}
