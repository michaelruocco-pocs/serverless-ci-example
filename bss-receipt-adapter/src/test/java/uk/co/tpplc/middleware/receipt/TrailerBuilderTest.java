package uk.co.tpplc.middleware.receipt;

import org.junit.Test;
import uk.co.tpplc.middleware.receipt.TrailerBuilder;

import static org.assertj.core.api.Assertions.assertThat;

public class TrailerBuilderTest {

    private final TrailerBuilder builder = new TrailerBuilder();

    @Test
    public void shouldBuildTrailer() {
        String trailer = builder.setLineCount(12)
                .setTotalValue(1234.56)
                .build();

        assertThat(trailer).isEqualTo("**EOF**~DATACOUNT~0000000012~ 000001234.56 ");
    }

}
