package uk.co.tpplc.middleware.receipt;

import uk.co.mruoc.properties.ClasspathFileContentLoader;
import uk.co.mruoc.properties.FileContentLoader;

public class TestLines {

    public static final ReceiptLine VALID_RECEIPT_LINE = loadValidLine();
    public static final ReceiptLine VALID_LEGACY_RECEIPT_LINE = loadValidLegacyLine();

    public static final String VALID_RECEIPT_LINE_TEXT = loadValidLineText();
    public static final String VALID_RECEIPT_LINE_XML = loadValidLineXml();

    private static final String VALID_RECEIPT_LINE_PATH = "/valid-receipt-line.txt";

    private static ReceiptLine loadValidLine() {
        return load(VALID_RECEIPT_LINE_PATH);
    }

    private static ReceiptLine loadValidLegacyLine() {
        return load("/valid-legacy-receipt-line.txt");
    }

    private static String loadValidLineText() {
        return loadContent(VALID_RECEIPT_LINE_PATH);
    }

    private static String loadValidLineXml() {
        return loadContent("/valid-receipt-line.xml");
    }

    private static ReceiptLine load(String path) {
        String content = loadContent(path);
        return toReceiptLine(content);
    }

    private static String loadContent(String path) {
        FileContentLoader contentLoader = new ClasspathFileContentLoader();
        return contentLoader.loadContent(path);
    }

    private static ReceiptLine toReceiptLine(String content) {
        ReceiptLineParser parser = new ReceiptLineParser();
        return parser.parse(content);
    }

}
