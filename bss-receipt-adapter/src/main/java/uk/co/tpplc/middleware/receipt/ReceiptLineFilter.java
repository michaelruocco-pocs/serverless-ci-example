package uk.co.tpplc.middleware.receipt;

import java.util.List;
import java.util.stream.Collectors;

public class ReceiptLineFilter {

    public List<ReceiptLine> filter(List<ReceiptLine> lines) {
        return lines.stream().filter(line -> !line.isLegacy()).collect(Collectors.toList());
    }

}
