package uk.co.tpplc.middleware.receipt;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.*;
import uk.co.mruoc.log.Logger;

public class SnsTopicClient implements TopicClient {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(SnsTopicClient.class);

    private final AmazonSNS sns;

    public SnsTopicClient() {
        this(AmazonSNSClientBuilder.standard().withRegion(Regions.EU_WEST_1).build());
    }

    public SnsTopicClient(AmazonSNS sns) {
        this.sns = sns;
    }

    @Override
    public void write(String topicArn, String message) {
        LOG.info().message("publishing message").field("messageBody", message).field("topic", topicArn).log();
        PublishResult result = sns.publish(topicArn, message);
        LOG.info().message("published message").field("messageBody", message).field("topic", topicArn).field("id", result.getMessageId()).log();
    }

}
