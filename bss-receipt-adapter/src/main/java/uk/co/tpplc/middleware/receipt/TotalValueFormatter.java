package uk.co.tpplc.middleware.receipt;

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;

public class TotalValueFormatter {

    private static final String DECIMAL_PATTERN = "#0.00";

    private static final int SIZE = 12;
    private static final char ZERO = '0';

    public static String format(double totalValue) {
        DecimalFormat formatter = new DecimalFormat(DECIMAL_PATTERN);
        String formatted = StringUtils.leftPad(formatter.format(totalValue), SIZE, ZERO);
        return " " + formatted + " ";
    }

}
