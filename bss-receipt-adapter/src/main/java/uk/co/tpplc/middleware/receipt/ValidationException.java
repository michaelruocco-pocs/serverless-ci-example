package uk.co.tpplc.middleware.receipt;

public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }

}
