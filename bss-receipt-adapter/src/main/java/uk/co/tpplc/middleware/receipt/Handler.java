package uk.co.tpplc.middleware.receipt;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import uk.co.mruoc.lambda.LambdaResponse;
import uk.co.mruoc.lambda.OkLambdaResponse;
import uk.co.mruoc.log.Logger;
import uk.co.mruoc.s3.ObjectDefinition;
import uk.co.mruoc.s3.S3EventConverter;
import uk.co.mruoc.s3.SimpleS3;

import java.util.List;

public class Handler implements RequestHandler<S3Event, LambdaResponse> {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(Handler.class);

    private final S3EventConverter s3EventConverter = new S3EventConverter();
    private final FileContentValidator fileContentValidator = new FileContentValidator();
    private final FileContentConverter fileContentConverter = new FileContentConverter();

    private final SimpleS3 simpleS3;
    private final LegacyReceiptLineFilter legacyFilter = new LegacyReceiptLineFilter();
    private final LegacyReceiptLinePublisher legacyPublisher;

    private final ReceiptLineFilter receiptFilter = new ReceiptLineFilter();
    private final ReceiptLinePublisher receiptLinePublisher;

    public Handler() {
        this(new DefaultEnvironment(), new ReceiptSimpleS3(LOG), new SnsTopicClient());
    }

    public Handler(Environment environment, SimpleS3 simpleS3, TopicClient topicClient) {
        this.simpleS3 = simpleS3;
        this.legacyPublisher = new LegacyReceiptLinePublisher(environment, simpleS3);
        this.receiptLinePublisher = new ReceiptLinePublisher(environment, topicClient);
    }

    @Override
    public LambdaResponse handleRequest(S3Event s3Event, Context context) {
        logEvent(s3Event, context);

        ObjectDefinition objectDefinition = s3EventConverter.toObjectDefinition(s3Event);
        String content = simpleS3.getObjectAsString(objectDefinition);
        fileContentValidator.validate(content);
        List<ReceiptLine> allLines = fileContentConverter.toReceiptLines(content);

        List<ReceiptLine> legacyLines = legacyFilter.filter(allLines);
        legacyPublisher.publish(objectDefinition.getKey(), legacyLines);

        List<ReceiptLine> receiptLines = receiptFilter.filter(allLines);
        receiptLinePublisher.publish(receiptLines);

        LambdaResponse response =  new OkLambdaResponse();
        logResponse(response, context);
        return response;
    }

    private void logEvent(S3Event s3Event, Context context) {
        LOG.info()
                .message("s3Event received")
                .field("s3Event", s3Event)
                .field("context", context)
                .log();
    }

    private void logResponse(LambdaResponse response, Context context) {
        LOG.info()
                .message("returning response")
                .field("response", response)
                .field("context", context)
                .log();
    }

}
