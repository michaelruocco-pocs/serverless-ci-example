package uk.co.tpplc.middleware.receipt;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class StringHandler {

    public boolean isValidNumber(String value) {
        return NumberUtils.isCreatable(cleanString(value));
    }

    public String cleanString(String value) {
        String cleanValue = value.trim();
        return StringUtils.stripStart(cleanValue, "0");
    }

}
