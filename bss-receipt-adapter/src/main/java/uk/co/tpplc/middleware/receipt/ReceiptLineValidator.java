package uk.co.tpplc.middleware.receipt;

public class ReceiptLineValidator {

    private static final int SIZE = 46;

    private final LineLengthValidator lineLengthValidator = new LineLengthValidator(SIZE);
    private final StringHandler stringHandler = new StringHandler();
    private final ReceiptLineParser receiptLineParser = new ReceiptLineParser();

    public boolean validate(String line) {
        if (!lineLengthValidator.isValid(line))
            throw new ValidationException("invalid receipt line: " + line + ": length should be " + SIZE);

        if (!stringHandler.isValidNumber(receiptLineParser.parseValue(line)))
            throw new ValidationException("invalid receipt line: " + line + ": value should be numeric");

        return true;
    }

}
