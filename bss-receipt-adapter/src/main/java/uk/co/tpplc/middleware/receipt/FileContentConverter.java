package uk.co.tpplc.middleware.receipt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileContentConverter {

    private static final String NEW_LINE = System.lineSeparator();
    private final ReceiptLineParser parser = new ReceiptLineParser();

    public List<ReceiptLine> toReceiptLines(String content) {
        List<String> lines = toLines(content);
        lines = removeTrailer(lines);
        List<ReceiptLine> receiptLines = new ArrayList<>();
        lines.forEach(line -> receiptLines.add(parser.parse(line)));
        return receiptLines;
    }

    private static List<String> toLines(String content) {
        return new ArrayList<>(Arrays.asList(content.split(NEW_LINE)));
    }

    private static List<String> removeTrailer(List<String> allLines) {
        List<String> lines = new ArrayList<>();
        lines.addAll(allLines);
        lines.remove(allLines.size() - 1);
        return lines;
    }

}
