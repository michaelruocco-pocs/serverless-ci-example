package uk.co.tpplc.middleware.receipt;

import java.util.List;
import java.util.stream.Collectors;

public class LegacyReceiptLineFilter {

    public List<ReceiptLine> filter(List<ReceiptLine> lines) {
        return lines.stream().filter(ReceiptLine::isLegacy).collect(Collectors.toList());
    }

}
