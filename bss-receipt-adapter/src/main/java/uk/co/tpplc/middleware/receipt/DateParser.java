package uk.co.tpplc.middleware.receipt;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateParser {

    private final DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");

    public DateTime parse(String value) {
        return format.parseDateTime(value);
    }

}
