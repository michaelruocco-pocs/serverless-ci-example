package uk.co.tpplc.middleware.receipt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileContentValidator {

    private static final String NEW_LINE = System.lineSeparator();

    private final TrailerLineValidator trailerLineValidator = new TrailerLineValidator();
    private final ReceiptLineValidator receiptLineValidator = new ReceiptLineValidator();

    private final TrailerLineParser trailerLineParser = new TrailerLineParser();
    private final ReceiptLineParser receiptLineParser = new ReceiptLineParser();

    public boolean validate(String content) {
        List<String> lines = toLines(content);
        String trailer = lines.remove(lines.size() - 1);
        trailerLineValidator.validate(trailer);

        validateLineCount(lines, trailer);

        double totalValue = 0;
        for (String line : lines) {
            receiptLineValidator.validate(line);
            totalValue += receiptLineParser.parseValueToDouble(line);
        }

        validateTotalValue(trailer, totalValue);

        return true;
    }

    private void validateLineCount(List<String> lines, String trailer) {
        int trailerLineCount = trailerLineParser.parseLineCountToInt(trailer);
        if (trailerLineCount != lines.size())
            throw new ValidationException("invalid file: number of lines " + lines.size() + " trailer has " + trailerLineCount);
    }

    private void validateTotalValue(String trailer, double totalValue) {
        double trailerTotalValue = trailerLineParser.parseTotalValueToDouble(trailer);
        if (trailerTotalValue != totalValue)
            throw new ValidationException("invalid file: total value " + totalValue + " trailer has " + trailerTotalValue);
    }

    private static List<String> toLines(String content) {
        return new ArrayList<>(Arrays.asList(content.split(NEW_LINE)));
    }

}
