package uk.co.tpplc.middleware.receipt;

import java.util.ArrayList;
import java.util.List;

public class TrailerBuilder {

    private static final String DELIMITER = "~";

    private double totalValue;
    private int lineCount;

    public TrailerBuilder setTotalValue(double totalValue) {
        this.totalValue = totalValue;
        return this;
    }

    public TrailerBuilder setLineCount(int lineCount) {
        this.lineCount = lineCount;
        return this;
    }

    public String build() {
        List<String> items = new ArrayList<>();
        items.add("**EOF**");
        items.add("DATACOUNT");
        items.add(LineCountFormatter.format(lineCount));
        items.add(TotalValueFormatter.format(totalValue));
        return String.join(DELIMITER, items);
    }

}
