package uk.co.tpplc.middleware.receipt;

import uk.co.mruoc.log.Logger;
import uk.co.mruoc.s3.ObjectDefinition;
import uk.co.mruoc.s3.SimpleS3;

import java.util.List;

public class LegacyReceiptLinePublisher {

    private static final Logger LOG = ReceiptLoggerFactory.getLogger(LegacyReceiptLinePublisher.class);

    private final Environment environment;
    private final SimpleS3 simpleS3;
    private final LegacyReceiptLineConverter converter;

    public LegacyReceiptLinePublisher(Environment environment, SimpleS3 simpleS3) {
        this(environment, simpleS3, new LegacyReceiptLineConverter());
    }

    public LegacyReceiptLinePublisher(Environment environment, SimpleS3 simpleS3, LegacyReceiptLineConverter converter) {
        this.environment = environment;
        this.simpleS3 = simpleS3;
        this.converter = converter;
    }

    public void publish(String objectKey, List<ReceiptLine> lines) {
        logInfo("publishing " + lines.size() + " legacy lines to bucket " + getBucketName());
        String content = converter.toContent(lines);

        ObjectDefinition objectDefinition = buildFileInfo(objectKey);
        simpleS3.putObject(objectDefinition, content);
    }

    private ObjectDefinition buildFileInfo(String objectKey) {
        String bucketName = getBucketName();
        return new ObjectDefinition(bucketName, objectKey);
    }

    private String getBucketName() {
        return environment.getBssLegacyReceiptBucketName();
    }

    private void logInfo(String message) {
        LOG.info().message(message).log();
    }

}
