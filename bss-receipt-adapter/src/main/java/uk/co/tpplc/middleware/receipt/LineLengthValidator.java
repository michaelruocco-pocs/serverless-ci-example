package uk.co.tpplc.middleware.receipt;

public class LineLengthValidator {

    private static final String DEFAULT_DELIMITER = "~";

    private final String delimiter;
    private final int size;

    public LineLengthValidator(int size) {
        this(DEFAULT_DELIMITER, size);
    }

    public LineLengthValidator(String delimiter, int size) {
        this.delimiter = delimiter;
        this.size = size;
    }

    public boolean isValid(String value) {
        String[] data = value.split(delimiter);
        return data.length == size;
    }

}
