package uk.co.tpplc.middleware.receipt;

public interface TopicClient {

    void write(String topicArn, String message);

}
