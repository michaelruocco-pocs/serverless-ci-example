package uk.co.tpplc.middleware.receipt;

import java.util.ArrayList;
import java.util.List;

public class LegacyReceiptLineConverter {

    private static final String NEW_LINE = System.lineSeparator();
    private final ReceiptLineParser parser = new ReceiptLineParser();

    public String toContent(List<ReceiptLine> lines) {
        String trailer = buildTrailer(lines);

        List<String> contentLines = new ArrayList<>();
        lines.forEach(line -> contentLines.add(line.getOriginalLine()));
        contentLines.add(trailer);

        return String.join(NEW_LINE, contentLines);
    }

    private String buildTrailer(List<ReceiptLine> lines) {
        double totalValue = calculateTotalValue(lines);
        int lineCount = lines.size();

        TrailerBuilder trailerBuilder = new TrailerBuilder();
        trailerBuilder.setLineCount(lineCount);
        trailerBuilder.setTotalValue(totalValue);
        return trailerBuilder.build();
    }

    private double calculateTotalValue(List<ReceiptLine> lines) {
        return lines.stream().mapToDouble(line -> parser.parseValueToDouble(line.getOriginalLine())).sum();
    }

}
