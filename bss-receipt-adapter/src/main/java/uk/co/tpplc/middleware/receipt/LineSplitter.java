package uk.co.tpplc.middleware.receipt;

public class LineSplitter {

    private static final String DELIMITER = "~";

    public String[] split(String value) {
        return value.split(DELIMITER);
    }

}
