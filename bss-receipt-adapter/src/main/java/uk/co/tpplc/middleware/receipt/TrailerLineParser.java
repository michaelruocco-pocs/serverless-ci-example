package uk.co.tpplc.middleware.receipt;

public class TrailerLineParser {

    private final LineSplitter lineSplitter = new LineSplitter();
    private final StringHandler stringHandler = new StringHandler();

    public String parseEof(String trailer) {
        String[] data = lineSplitter.split(trailer);
        return data[0];
    }

    public String parseDataCount(String trailer) {
        String[] data = lineSplitter.split(trailer);
        return data[1];
    }

    public String parseLineCount(String trailer) {
        String[] data = lineSplitter.split(trailer);
        return stringHandler.cleanString(data[2]);
    }

    public int parseLineCountToInt(String trailer) {
        return Integer.parseInt(parseLineCount(trailer));
    }

    public String parseTotalValue(String trailer) {
        String[] data = lineSplitter.split(trailer);
        return stringHandler.cleanString(data[3]);
    }

    public double parseTotalValueToDouble(String trailer) {
        return Double.parseDouble(parseTotalValue(trailer));
    }

}
