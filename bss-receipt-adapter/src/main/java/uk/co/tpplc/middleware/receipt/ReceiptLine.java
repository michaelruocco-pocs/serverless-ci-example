package uk.co.tpplc.middleware.receipt;

import org.joda.time.DateTime;

public class ReceiptLine {

    private final String originalLine;
    private final String purchaseOrderId;
    private final DateTime actualDeliveryDate;
    private final DateTime documentDate;
    private final String externalGrn;
    private final double receivedQuantity;
    private final String supplierId;
    private final String idType;

    public ReceiptLine(ReceiptLineBuilder builder) {
        this.originalLine = builder.originalLine;
        this.purchaseOrderId = builder.purchaseOrderId;
        this.actualDeliveryDate = builder.actualDeliveryDate;
        this.documentDate = builder.documentDate;
        this.externalGrn = builder.externalGrn;
        this.receivedQuantity = builder.receivedQuantity;
        this.supplierId = builder.supplierId;
        this.idType = builder.purchaseOrderIdType;
    }

    public String getOriginalLine() {
        return originalLine;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public boolean isLegacy() {
        return purchaseOrderId.length() <= 6;
    }

    public DateTime getActualDeliveryDate() {
        return actualDeliveryDate;
    }

    public DateTime getDocumentDate() {
        return documentDate;
    }

    public String getExternalGrn() {
        return externalGrn;
    }

    public double getReceivedQuantity() {
        return receivedQuantity;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public String getPurchaseOrderIdType() {
        return idType;
    }

    public static class ReceiptLineBuilder {

        private String originalLine;
        private String purchaseOrderId;
        private DateTime actualDeliveryDate;
        private DateTime documentDate;
        private String externalGrn;
        private double receivedQuantity;
        private String supplierId;
        private String purchaseOrderIdType;

        public ReceiptLineBuilder setOriginalLine(String originalLine) {
            this.originalLine = originalLine;
            return this;
        }

        public ReceiptLineBuilder setPurchaseOrderId(String purchaseOrderId) {
            this.purchaseOrderId = purchaseOrderId;
            return this;
        }

        public ReceiptLineBuilder setActualDeliveryDate(DateTime actualDeliveryDate) {
            this.actualDeliveryDate = actualDeliveryDate;
            return this;
        }

        public ReceiptLineBuilder setDocumentDate(DateTime documentDate) {
            this.documentDate = documentDate;
            return this;
        }

        public ReceiptLineBuilder setExternalGrn(String externalGrn) {
            this.externalGrn = externalGrn;
            return this;
        }

        public ReceiptLineBuilder setReceivedQuantity(double receivedQuantity) {
            this.receivedQuantity = receivedQuantity;
            return this;
        }

        public ReceiptLineBuilder setSupplierId(String supplierId) {
            this.supplierId = supplierId;
            return this;
        }

        public ReceiptLineBuilder setPurchaseOrderIdType(String purchaseOrderIdType) {
            this.purchaseOrderIdType = purchaseOrderIdType;
            return this;
        }

        public ReceiptLine build() {
            return new ReceiptLine(this);
        }

    }

}
