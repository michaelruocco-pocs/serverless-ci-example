package uk.co.tpplc.middleware.receipt;

import java.util.ArrayList;
import java.util.List;

public class ReceiptLinePublisher {

    private final Environment environment;
    private final ReceiptLineConverter converter = new ReceiptLineConverter();
    private final TopicClient topicClient;

    public ReceiptLinePublisher(Environment environment, TopicClient topicClient) {
        this.environment = environment;
        this.topicClient = topicClient;
    }

    public void publish(List<ReceiptLine> receiptLines) {
        List<String> lines = toStrings(receiptLines);
        String topicName = getTopicName();
        for (String line : lines)
            topicClient.write(topicName, line);
    }

    private List<String> toStrings(List<ReceiptLine> receiptLines) {
        List<String> lines = new ArrayList<>();
        for (ReceiptLine receiptLine : receiptLines)
            lines.add(converter.toXml(receiptLine));
        return lines;
    }

    private String getTopicName() {
        return environment.getCmmReceiptTopicArn();
    }

}
