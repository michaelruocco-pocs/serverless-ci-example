package uk.co.tpplc.middleware.receipt;


public class TrailerLineValidator {

    private static final int SIZE = 4;
    private static final String EOF = "**EOF**";
    private static final String DATACOUNT = "DATACOUNT";

    private final LineLengthValidator lineLengthValidator = new LineLengthValidator(SIZE);
    private final TrailerLineParser parser = new TrailerLineParser();
    private final StringHandler stringHandler = new StringHandler();

    public boolean validate(String trailer) {
        if (!lineLengthValidator.isValid(trailer))
            throw new ValidationException("invalid trailer: " + trailer + ": length should be " + SIZE);

        if (!isEOF(parser.parseEof(trailer)))
            throw new ValidationException("invalid trailer: " + trailer + ": first entry should be " + EOF);

        if (!isDataCount(parser.parseDataCount(trailer)))
            throw new ValidationException("invalid trailer: " + trailer + ": second entry should be " + DATACOUNT);

        if (!stringHandler.isValidNumber(parser.parseLineCount(trailer)))
            throw new ValidationException("invalid trailer: " + trailer + ": line count should be numeric");

        if (!stringHandler.isValidNumber(parser.parseTotalValue(trailer)))
            throw new ValidationException("invalid trailer: " + trailer + ": total value should be numeric");

        return true;
    }

    private boolean isEOF(String value) {
        String cleanValue = stringHandler.cleanString(value);
        return EOF.equals(cleanValue);
    }

    private boolean isDataCount(String value) {
        String cleanValue = stringHandler.cleanString(value);
        return DATACOUNT.equals(cleanValue);
    }

}
