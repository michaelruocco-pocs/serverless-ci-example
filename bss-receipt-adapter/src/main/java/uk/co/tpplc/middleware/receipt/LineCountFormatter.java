package uk.co.tpplc.middleware.receipt;

import org.apache.commons.lang3.StringUtils;

public class LineCountFormatter {

    private static final int SIZE = 10;
    private static final char ZERO = '0';

    public static String format(int lineCount) {
        return StringUtils.leftPad(Integer.toString(lineCount), SIZE, ZERO);
    }

}
