package uk.co.tpplc.middleware.receipt;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

public class ReceiptLineConverter {

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    public String toXml(ReceiptLine line) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();
            Element root = document.createElement("DeliveryReceipt");
            document.appendChild(root);

            Element deliveryDate = document.createElement("ActualDeliveryDateTime");
            deliveryDate.appendChild(document.createTextNode(line.getActualDeliveryDate().toString(DATE_FORMAT)));
            root.appendChild(deliveryDate);

            Element documentDate = document.createElement("DocumentDateTime");
            documentDate.appendChild(document.createTextNode(line.getDocumentDate().toString(DATE_FORMAT)));
            root.appendChild(documentDate);

            Element externalGrn = document.createElement("ExternalGRN");
            externalGrn.appendChild(document.createTextNode(line.getExternalGrn()));
            root.appendChild(externalGrn);

            Element deliveryReceiptLine = document.createElement("DeliveryReceiptLine");
            root.appendChild(deliveryReceiptLine);

            Element receivedQuantity = document.createElement("ReceivedQuantity");
            receivedQuantity.appendChild(document.createTextNode(Double.toString(line.getReceivedQuantity())));
            deliveryReceiptLine.appendChild(receivedQuantity);

            Element purchaseOrderLineItem = document.createElement("PurchaseOrderLineItem");
            deliveryReceiptLine.appendChild(purchaseOrderLineItem);

            Element lineNumber = document.createElement("LineNumber");
            lineNumber.appendChild(document.createTextNode("1"));
            purchaseOrderLineItem.appendChild(lineNumber);

            Element purchaseOrder = document.createElement("PurchaseOrder");
            deliveryReceiptLine.appendChild(purchaseOrder);

            Element supplier = document.createElement("Supplier");
            purchaseOrder.appendChild(supplier);

            Element supplierId = document.createElement("Id");
            supplierId.appendChild(document.createTextNode(line.getSupplierId()));
            supplier.appendChild(supplierId);

            Element purchaseOrderIds = document.createElement("PurchaseOrderIds");
            purchaseOrder.appendChild(purchaseOrderIds);

            Element purchaseOrderId = document.createElement("Id");
            purchaseOrderId.appendChild(document.createTextNode(line.getPurchaseOrderId()));
            purchaseOrderIds.appendChild(purchaseOrderId);

            Element purchaseOrderIdType = document.createElement("IdType");
            purchaseOrderIdType.appendChild(document.createTextNode(line.getPurchaseOrderIdType()));
            purchaseOrderIds.appendChild(purchaseOrderIdType);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(document);
            transformer.transform(source, result);
            return writer.toString();
        } catch (ParserConfigurationException | TransformerException e) {
            throw new RuntimeException(e);
        }
    }

}
