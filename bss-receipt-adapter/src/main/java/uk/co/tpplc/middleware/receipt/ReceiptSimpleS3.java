package uk.co.tpplc.middleware.receipt;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import uk.co.mruoc.log.Logger;
import uk.co.mruoc.s3.JsonLogSimpleS3;

public class ReceiptSimpleS3 extends JsonLogSimpleS3 {

    public ReceiptSimpleS3(Logger logger) {
        super(build(), logger);
    }

    private static AmazonS3 build() {
        return AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_1).build();
    }

}
