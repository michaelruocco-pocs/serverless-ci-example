package uk.co.tpplc.middleware.receipt;

import org.joda.time.DateTime;
import uk.co.tpplc.middleware.receipt.ReceiptLine.ReceiptLineBuilder;

public class ReceiptLineParser {

    private final LineSplitter lineSplitter = new LineSplitter();
    private final StringHandler stringHandler = new StringHandler();
    private final DateParser dateParser = new DateParser();

    public ReceiptLine parse(String line) {
        return new ReceiptLineBuilder()
                .setOriginalLine(line)
                .setPurchaseOrderId(parsePurchaseOrderId(line))
                .setActualDeliveryDate(parseActualDeliveryDate(line))
                .setDocumentDate(parseDocumentDate(line))
                .setExternalGrn(parseExternalGrn(line))
                .setReceivedQuantity(parseReceivedQuantityToDouble(line))
                .setSupplierId(parseSupplierId(line))
                .setPurchaseOrderIdType(parsePurchaseOrderIdType(line))
                .build();
    }

    public String parseValue(String line) {
        String[] data = lineSplitter.split(line);
        return stringHandler.cleanString(data[7]);
    }

    public double parseValueToDouble(String line) {
        return Double.parseDouble(parseValue(line));
    }

    private String parsePurchaseOrderId(String line) {
        String[] data = lineSplitter.split(line);
        return data[21].trim();
    }

    private DateTime parseActualDeliveryDate(String line) {
        String[] data = lineSplitter.split(line);
        String value = data[9];
        return dateParser.parse(value);
    }

    private DateTime parseDocumentDate(String line) {
        String[] data = lineSplitter.split(line);
        String value = data[12];
        return dateParser.parse(value);
    }

    private String parseExternalGrn(String line) {
        String[] data = lineSplitter.split(line);
        return data[8].trim();
    }

    private String parseReceivedQuantity(String line) {
        String[] data = lineSplitter.split(line);
        return stringHandler.cleanString(data[27]);
    }

    private double parseReceivedQuantityToDouble(String line) {
        return Double.parseDouble(parseReceivedQuantity(line));
    }

    private String parseSupplierId(String line) {
        String[] data = lineSplitter.split(line);
        return data[35].trim();
    }

    private String parsePurchaseOrderIdType(String line) {
        String[] data = lineSplitter.split(line);
        return data[22].trim();
    }

}
